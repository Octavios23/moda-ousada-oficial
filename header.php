<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moda Ousada - Store</title>

    <link rel="shortcut icon" href="https://modaousada.com/img/icone.png" >

    <meta name="description" content=" Loja Virtual de Artigos Íntimos Femininos e moda feminina. Maricá, RJ - Brasil. Acesse já">
    <meta name="robots" content="index">
    <meta name="author" content="#ArtigosíntimosFemininos">
    <meta name="keywords" content="Moda íntima, sutiã, calcinha, lingerie, feminino">

    <meta property="og:type" content="page">
    <meta property="og:url" content="https://instagram.com/ousadamodaintima98?igshid=1qxor8nsnjxmv">
    <meta property="og:title" content="Moda Ousada">
    <meta property="og:image" content="https://modaousada.com/img/icone.png">
    <meta property="og:description" content="Loja Virtual de Artigos Íntimos Femininos e moda feminina. Maricá, RJ - Brasil. Acesse já">

    <meta property="article:author" content="Moda Ousada">


     <!-- CSS -->        
    <link rel="stylesheet" href="./assets/css/template-ousada.css">
                    
  

</head>
<body onunload="window.opener.location.reload();">


    
