<section id="promotion" data-animation="five" data-status="off">
    <div class="container-fluid py-5" style="background-color: black;">
        <div class="row p-2 p-md-5" style="background: black url(./assets/img/background-section.jpg) no-repeat center right scroll; background-size: contain;">
            <div class="col-12 col-xl-11 m-md-5 to-campaign " style="padding: 2vh 2vw">
               <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="box-title me-5 animate-8" data-action="1">
                                <h1 class="text-white" style="letter-spacing: 1.5vw;">SINTA-SE AVONTADE DO SEU JEITO</h1>
                            </div>
                            <div class="box-text w-75 w-md-50 pe-5 py-2 text-white animate-9" data-action="1">
                                <p style="line-height: 1.5;">Uma lingerie nova sempre é uma boa ideia. Conheça nossos lançamentos e modelos de verão para dar um novo look no seu guarda-roupas</p>
                            </div>
                            <div class="box-button to-left">
                                <div class="button">
                                    <a href="https://instagram.com/ousadamodaintima98?igshid=1qxor8nsnjxmv">
                                        Acompanhe as novidades
                                        <div class="box-icon" style="display: inline;">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve"><g>
                                            <g xmlns="http://www.w3.org/2000/svg">
                                            <g><path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160    C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48    h192c61.76,0,112,50.24,112,112V352z" fill="#ffffff" data-original="#000000" style=""/></g>
                                            </g><g xmlns="http://www.w3.org/2000/svg"><g><path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336    c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"><g><circle cx="393.6" cy="118.4" r="17.056" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>                     
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</section>