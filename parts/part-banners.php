<section id="newslatter" data-animation="two" data-status="off">
    <div class="container-fluid" style="background-color:  #0f0f0f;" >
        <div class="row py-3 justify-content-center" data-animation="three" data-status="off">
            <div class="col-12 col-xl-11">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 py-5">
                            <div  class="box-title to-carousel-produtcs animate-3" data-action="1">
                                <h2>SINTA-SE BEM EM TODOS OS MOMENTOS</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-xl-11">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="caousel-showcase animate-2" data-action="1">
                                <div class="owl-carousel owl-theme"> <!-- Estrutura do Carrossel -->
                                    <div class="item to-carousel-box">
                                        <div class="box-img">
                                            <img src="./assets/img/carouselProduct1.jpeg" alt="First Slider">
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div class="box-img">
                                            <img src="./assets/img/carouselProduct2.jpg" alt="Second Slider"> 
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div  class="box-img">
                                            <img src="./assets/img/carouselProduct3.jpeg" alt="Third Slider" data-img="1">
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div  class="box-img">
                                            <img src="./assets/img/carouselProduct4.jpg" alt="Fourth Slider" data-img="2">
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div class="box-img">
                                            <img src="./assets/img/carouselProduct5.jpg" alt="Fifth Slider" data-img="3">
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div  class="box-img">
                                            <img src="./assets/img/carouselProduct6.jpg" alt="Sixth Slider" data-img="4">
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div class="box-img">
                                            <img src="./assets/img/carouselProduct7.jpeg" alt="Seventh Slider" data-img="5">
                                        </div>
                                    </div>
                                    <div class="item to-carousel-box">
                                        <div  class="box-img">
                                            <img src="./assets/img/carouselProduct8.jpg" alt="Eighth Slider" data-img="6">
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
