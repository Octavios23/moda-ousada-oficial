<header>
        <div class="container-fluid">
            <div class="row bg-dark p-1 in-down justify-content-center"> <!-- TOP HEADER -->
                <div class="col-12 col-xl-11">
                    <div class="container">
                        <div class="row">
                            <div class="col-6">
                                <div class="info-box">
                                    <div class="box-icon box-map">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 682 682.66669" style="enable-background:new 0 0 512 512" xml:space="preserve"><g><path xmlns="http://www.w3.org/2000/svg" d="m216.210938 0c-122.664063 0-222.460938 99.796875-222.460938 222.460938 0 154.175781 222.679688 417.539062 222.679688 417.539062s222.242187-270.945312 222.242187-417.539062c0-122.664063-99.792969-222.460938-222.460937-222.460938zm67.121093 287.597656c-18.507812 18.503906-42.8125 27.757813-67.121093 27.757813-24.304688 0-48.617188-9.253907-67.117188-27.757813-37.011719-37.007812-37.011719-97.226562 0-134.238281 17.921875-17.929687 41.761719-27.804687 67.117188-27.804687 25.355468 0 49.191406 9.878906 67.121093 27.804687 37.011719 37.011719 37.011719 97.230469 0 134.238281zm0 0" fill="#ffffff" data-original="#000000"/></g></svg>
                                    </div>
                                    <div class="box-text">
                                        <span>Maricá, RJ - Brasil</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 pe-4">
                                <div class="info-box float-right">
                                    <div class="box-icon box-acert">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 384 384" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg"><g><path d="M353.188,252.052c-23.51,0-46.594-3.677-68.469-10.906c-10.719-3.656-23.896-0.302-30.438,6.417l-43.177,32.594    c-50.073-26.729-80.917-57.563-107.281-107.26l31.635-42.052c8.219-8.208,11.167-20.198,7.635-31.448    c-7.26-21.99-10.948-45.063-10.948-68.583C132.146,13.823,118.323,0,101.333,0H30.813C13.823,0,0,13.823,0,30.813    C0,225.563,158.438,384,353.188,384c16.99,0,30.813-13.823,30.813-30.813v-70.323C384,265.875,370.177,252.052,353.188,252.052z" fill="#ffffff" data-original="#000000" style="" class=""/></g></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>
                                    </div>
                                    <div class="box-text">
                                        <a href="tel:+55 (21) 9 9662-1073"><span>CONTATO: (21) 9 9662-1073</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-1 pt-md-0 bg-white justify-content-center"> <!-- MENU NAV -->
                <div class="col-12 col-xl-11">
                    <div class="container px-0">
                        <div class="row">
                            <div class="col-5 col-md-3 ">
                                <div class="box-logo pe-lg-4 pe-xl-5 in-left">
                                    <img class="float-right" src="./assets/img/logo.png" alt="logotipo">
                                </div>
                            </div>
                            <div class="col-7 col-md-6 px-0 ms-auto text-right">
                                <nav class="navbar-toggle">
                                    <button class="bg-dark js-btn-toggle" type="button" data-toggle="collapse">
                                            <span class="el1"></span><span class="el2"></span><span class="el3"></span>
                                    </button>
                                    <ul class="navbar">
                                        <li><a href="https://modaousada.com/#product">Produtos</a></li>
                                        <li><a href="https://modaousada.com/#newslatter">Novidades</a></li>
                                        <li><a href="https://modaousada.com/#promotion">Promoções</a></li>
                                        <li><a href="https://modaousada.com/#contact">Contato</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-0 col-md-3 text-center">
                                <div class="box-button">
                                    <a href="https://api.whatsapp.com/send?phone=+5521996621073&text=Olá,+poderia+me+falar+mais+sobre+seus+produtos?">PEÇA JÁ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>