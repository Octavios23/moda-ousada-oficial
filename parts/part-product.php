<section id="product" data-animation="four" data-status="off">
        <div class="container-fluid">
            <div class="row pb-5">
                <div class="col-12 pb-5 px-0">
                    <div class="container">
                        <div class="row py-5 justify-content-center">
                            <div class="col-7 col-xl-12 pt-5 pb-3">
                                <div class="box-title to-product-header animate-4" data-action="1">
                                    <h2 class="title">NOSSA LOJA</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12" style="display: flex;">
                                <div id="categ-on" class="js-box-categ box-categ me-xl-2 animate-5" data-categoria="OS MAIS VENDIDOS" data-action="1">
                                    <div class="info-box">
                                        <div class="box-icon">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="gifts" class="svg-inline--fa fa-gifts fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M240.6 194.1c1.9-30.8 17.3-61.2 44-79.8C279.4 103.5 268.7 96 256 96h-29.4l30.7-22c7.2-5.1 8.9-15.1 3.7-22.3l-9.3-13c-5.1-7.2-15.1-8.9-22.3-3.7l-32 22.9 11.5-30.6c3.1-8.3-1.1-17.5-9.4-20.6l-15-5.6c-8.3-3.1-17.5 1.1-20.6 9.4l-19.9 53-19.9-53.1C121 2.1 111.8-2.1 103.5 1l-15 5.6C80.2 9.7 76 19 79.2 27.2l11.5 30.6L58.6 35c-7.2-5.1-17.2-3.5-22.3 3.7l-9.3 13c-5.1 7.2-3.5 17.2 3.7 22.3l30.7 22H32c-17.7 0-32 14.3-32 32v352c0 17.7 14.3 32 32 32h168.9c-5.5-9.5-8.9-20.3-8.9-32V256c0-29.9 20.8-55 48.6-61.9zM224 480c0 17.7 14.3 32 32 32h160V384H224v96zm224 32h160c17.7 0 32-14.3 32-32v-96H448v128zm160-288h-20.4c2.6-7.6 4.4-15.5 4.4-23.8 0-35.5-27-72.2-72.1-72.2-48.1 0-75.9 47.7-87.9 75.3-12.1-27.6-39.9-75.3-87.9-75.3-45.1 0-72.1 36.7-72.1 72.2 0 8.3 1.7 16.2 4.4 23.8H256c-17.7 0-32 14.3-32 32v96h192V224h15.3l.7-.2.7.2H448v128h192v-96c0-17.7-14.3-32-32-32zm-272 0c-2.7-1.4-5.1-3-7.2-4.8-7.3-6.4-8.8-13.8-8.8-19 0-9.7 6.4-24.2 24.1-24.2 18.7 0 35.6 27.4 44.5 48H336zm199.2-4.8c-2.1 1.8-4.5 3.4-7.2 4.8h-52.6c8.8-20.3 25.8-48 44.5-48 17.7 0 24.1 14.5 24.1 24.2 0 5.2-1.5 12.6-8.8 19z"></path></svg>
                                        </div>
                                        <div class="box-text">
                                            <span>OS MAIS VENDIDOS</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-box-categ box-categ mx-xl-2 animate-6" data-categoria="NOSSAS NOVIDADES" data-action="1">
                                    <div class="info-box">
                                        <div class="box-icon">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                        </div>
                                        <div class="box-text">
                                            <span>NOSSAS NOVIDADES</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-box-categ box-categ ms-xl-2 animate-7" data-categoria="NOSSAS PROMOÇÕES" data-action="1">
                                    <div class="info-box">
                                        <div class="box-icon">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tags" class="svg-inline--fa fa-tags fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M497.941 225.941L286.059 14.059A48 48 0 0 0 252.118 0H48C21.49 0 0 21.49 0 48v204.118a48 48 0 0 0 14.059 33.941l211.882 211.882c18.744 18.745 49.136 18.746 67.882 0l204.118-204.118c18.745-18.745 18.745-49.137 0-67.882zM112 160c-26.51 0-48-21.49-48-48s21.49-48 48-48 48 21.49 48 48-21.49 48-48 48zm513.941 133.823L421.823 497.941c-18.745 18.745-49.137 18.745-67.882 0l-.36-.36L527.64 323.522c16.999-16.999 26.36-39.6 26.36-63.64s-9.362-46.641-26.36-63.64L331.397 0h48.721a48 48 0 0 1 33.941 14.059l211.882 211.882c18.745 18.745 18.745 49.137 0 67.882z"></path></svg>  
                                        </div>
                                        <div class="box-text">
                                            <span>NOSSAS PROMOÇÕES</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row mt-5">
                            <div class="col-10 pt-5">
                            <div class="box-product" style="display: flex;flex-wrap: wrap;">
                            
                                 <!-- BACKEND  PRODUTOS -->
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>