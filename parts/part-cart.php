<div class="cart" >
    <div class="box-header p-1">
        <h6>Meu Carrinho</h6>
    </div>
    <div class="cart-content">
        <div class="info-table  mx-2">
            <table>
                <thead>
                    <tr>
                        <th>QTD</th>
                        <th>Produto</th>
                        <th>Tamanho</th>
                        <th>R$</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="cart-checkout">
                    
                </tbody>
            </table>
            </div>
            <div class="box-result me-5 ms-4 my-2 px-3">
                <div class="box-indices">
                    <div class="sub">
                        <span>SUBTOTAL:</span>
                    </div>
                    <div class="descont">
                        <span>DESCONTOS:</span>
                    </div>
                    <div class="tot">
                        <span>TOTAL:</span>
                    </div>
                </div>
                <div class="box-values">
                    <div class="sub-val">
                        <p class="text-danger">R$<span data-value="sub">00,00</span></p>
                    </div>
                    <div class="descont text-right">
                        <span data-value="des">15%</span>
                    </div>
                    <div class="tot">
                        <p>R$<span data-value="tot">00,00</span></p>
                    </div>
                </div>
            </div>
            <div class="box-send mx-2 mt-4 mb-2">
                <div class="box-button mt-0 mb-3 text-center">
                    <a>
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cash-register" class="svg-inline--fa fa-cash-register fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M511.1 378.8l-26.7-160c-2.6-15.4-15.9-26.7-31.6-26.7H208v-64h96c8.8 0 16-7.2 16-16V16c0-8.8-7.2-16-16-16H48c-8.8 0-16 7.2-16 16v96c0 8.8 7.2 16 16 16h96v64H59.1c-15.6 0-29 11.3-31.6 26.7L.8 378.7c-.6 3.5-.9 7-.9 10.5V480c0 17.7 14.3 32 32 32h448c17.7 0 32-14.3 32-32v-90.7c.1-3.5-.2-7-.8-10.5zM280 248c0-8.8 7.2-16 16-16h16c8.8 0 16 7.2 16 16v16c0 8.8-7.2 16-16 16h-16c-8.8 0-16-7.2-16-16v-16zm-32 64h16c8.8 0 16 7.2 16 16v16c0 8.8-7.2 16-16 16h-16c-8.8 0-16-7.2-16-16v-16c0-8.8 7.2-16 16-16zm-32-80c8.8 0 16 7.2 16 16v16c0 8.8-7.2 16-16 16h-16c-8.8 0-16-7.2-16-16v-16c0-8.8 7.2-16 16-16h16zM80 80V48h192v32H80zm40 200h-16c-8.8 0-16-7.2-16-16v-16c0-8.8 7.2-16 16-16h16c8.8 0 16 7.2 16 16v16c0 8.8-7.2 16-16 16zm16 64v-16c0-8.8 7.2-16 16-16h16c8.8 0 16 7.2 16 16v16c0 8.8-7.2 16-16 16h-16c-8.8 0-16-7.2-16-16zm216 112c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h176c4.4 0 8 3.6 8 8v16zm24-112c0 8.8-7.2 16-16 16h-16c-8.8 0-16-7.2-16-16v-16c0-8.8 7.2-16 16-16h16c8.8 0 16 7.2 16 16v16zm48-80c0 8.8-7.2 16-16 16h-16c-8.8 0-16-7.2-16-16v-16c0-8.8 7.2-16 16-16h16c8.8 0 16 7.2 16 16v16z"></path></svg>
                    Fazer Pedido </a>
                </div>
                <div class="box-text">
                    <span>Ao finalizar, clique no botão para ser encaminhado a um vendedor</span>
                </div>
            </div>                
        </div>
    </div>
</div> 