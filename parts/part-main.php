<main class="elemento"  data-status="off" data-animation="one">
    <div class="container-fluid px-0">
        <div class="row m-0">
            <div class="col-12 px-0 in-bottom-left">
                <div class="carousel">
                    <div id="carouselOne" class="carousel slide carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="background-m1">
                                    <div class="layer">
                                        <div class="container-fluid px-0">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-xl-11">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="box-text text-light" style="margin-bottom: 80px;opacity: 1 !important;">
                                                                    <h1 class="text-center font-weight-bold text-light">COMPLETE<br>O SEU LOOK</h1>
                                                                </div>
                                                                <div class="box-button animate-1" data-action='1'>
                                                                    <a class="p-3" href="https://instagram.com/ousadamodaintima98?igshid=1qxor8nsnjxmv" style="font-size: 1.8vw;background-color: #840731" >
                                                                        Acompanhe as Novidades
                                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve"><g>
                                                                        <g xmlns="http://www.w3.org/2000/svg">
                                                                        <g><path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160    C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48    h192c61.76,0,112,50.24,112,112V352z" fill="#ffffff" data-original="#000000" style=""/></g>
                                                                        </g><g xmlns="http://www.w3.org/2000/svg"><g><path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336    c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"><g><circle cx="393.6" cy="118.4" r="17.056" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="background-m2">
                                    <div class="layer">
                                        <div class="container-fluid px-0">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-xl-11">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="box-text text-light" style="margin-bottom: 80px;opacity: 1 !important;">
                                                                    <h1 class="text-center font-weight-bold text-light">COMPLETE<br>O SEU LOOK</h1>
                                                                </div>
                                                                <div class="box-button animate-1" data-action='1'>
                                                                    <a class="p-3" href="https://instagram.com/ousadamodaintima98?igshid=1qxor8nsnjxmv" style="font-size: 1.8vw;background-color: #840731" >
                                                                        Acompanhe as Novidades
                                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve"><g>
                                                                        <g xmlns="http://www.w3.org/2000/svg">
                                                                        <g><path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160    C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48    h192c61.76,0,112,50.24,112,112V352z" fill="#ffffff" data-original="#000000" style=""/></g>
                                                                        </g><g xmlns="http://www.w3.org/2000/svg"><g><path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336    c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"><g><circle cx="393.6" cy="118.4" r="17.056" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="background-m3">
                                    <div class="layer">
                                        <div class="container-fluid px-0">
                                            <div class="row justify-content-center">
                                                <div class="col-12 col-xl-11">
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="box-text text-light" style="margin-bottom: 80px;opacity: 1 !important;">
                                                                    <h1 class="text-center font-weight-bold text-light">COMPLETE<br>O SEU LOOK</h1>
                                                                </div>
                                                                <div class="box-button animate-1" data-action='1'>
                                                                    <a class="p-3" href="https://instagram.com/ousadamodaintima98?igshid=1qxor8nsnjxmv" style="font-size: 1.8vw;background-color: #840731" >
                                                                        Acompanhe as Novidades
                                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve"><g>
                                                                        <g xmlns="http://www.w3.org/2000/svg">
                                                                        <g><path d="M352,0H160C71.648,0,0,71.648,0,160v192c0,88.352,71.648,160,160,160h192c88.352,0,160-71.648,160-160V160    C512,71.648,440.352,0,352,0z M464,352c0,61.76-50.24,112-112,112H160c-61.76,0-112-50.24-112-112V160C48,98.24,98.24,48,160,48    h192c61.76,0,112,50.24,112,112V352z" fill="#ffffff" data-original="#000000" style=""/></g>
                                                                        </g><g xmlns="http://www.w3.org/2000/svg"><g><path d="M256,128c-70.688,0-128,57.312-128,128s57.312,128,128,128s128-57.312,128-128S326.688,128,256,128z M256,336    c-44.096,0-80-35.904-80-80c0-44.128,35.904-80,80-80s80,35.872,80,80C336,300.096,300.096,336,256,336z" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"><g><circle cx="393.6" cy="118.4" r="17.056" fill="#ffffff" data-original="#000000" style=""/></g></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g><g xmlns="http://www.w3.org/2000/svg"></g></g></svg>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <a class="carousel-control-prev" href="#carouselOne" role="button" data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselOne" role="button" data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-0 justify-content-center" style="background-color: #840731;">
            <div class="col-6 col-md-3">
                <div class="carousel-tags">
                    <div id="carouselTwo" class="carousel slide carousel" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active h-100">
                                <img class="d-block w-100 w-md-75 mx-auto" src="./assets/img/carouseltag1.png" alt="slide first">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 w-md-75 mx-auto" src="./assets/img/carouseltag2.png" alt="slide two">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100 w-md-75 mx-auto" src="./assets/img/carouseltag3.png" alt="slide third">
                            </div>
                        
                            <!--<a class="carousel-control-prev" href="#carouselTwo" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselTwo" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                            </a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
