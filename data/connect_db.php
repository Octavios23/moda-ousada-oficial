<?php
//Connect Database

$servername = 'localhost';
$username = 'root';
$password = '';
$bd_name = 'modaousada';

try{
    $pdo = new PDO('mysql:host=localhost;dbname='.$bd_name, $username,$password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo "Error: ".$e->getMessage();
}

global $pdo;