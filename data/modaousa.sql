// CRIAÇÃO DO BANCO DE DADOS
CREATE DATABASE modaousada CHARACTER SET utf8 COLLATE utf8_unicode_ci;


// CRIAÇÃO DA TABELA DO ADMINSTRADOR
CREATE TABLE IF NOT EXISTS ig_adminstrador(
    id_admin            int NOT NULL AUTO_INCREMENT,
    nome                char(255) NOT NULL,
    user                char(10) NOT NULL,
    telefone            char(11) NOT NULL,
    email               char(255) NOT NULL,
    senha               char(255) NOT NULL,
    desconto            char(255) NOT NULL,
    gtag                char(255),
    pix_facebook        char(255),
    CONSTRAINT pk_id_admin PRIMARY KEY (id_admin)
);

// CRIAÇÃO DA TABELA DE PRODUTOS
CREATE TABLE IF NOT EXISTS ig_produtos(
    id_produto          int NOT NULL AUTO_INCREMENT,
    nome                char(255) NOT NULL, 
    tamanho             char(150) NOT NULL, 
    preco               float NOT NULL, 
    img                 text(500) NOT NULL, 
    categoria           char(255) NOT NULL, 
    disponibilidade     boolean(1) DEFAULT 0,
    privacidade         boolean(1) DEFAULT 0,
    CONSTRAINT pk_id_produto PRIMARY KEY (id_produto)
);

// CRIAÇÃO DA TABELA DE CLIENTES
CREATE TABLE IF NOT EXISTS ig_clientes(
    id_clientes         int NOT NULL AUTO_INCREMENT, 
    email               char(255) NOT NULL,
    celular             char(20)  NOT NULL, 
    terms               char(20)  NOT NULL,
    localizacao         char(255),
    data_register       DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_id_clientes PRIMARY KEY (id_clientes)
);
  