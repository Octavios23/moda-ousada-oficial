var Ajax = function(){
  let $loc = new Array();

  this.categoria = function($categ){
    document.addEventListener("DOMContentLoaded", ()=>{
      $loc[0] = document.querySelectorAll('.box-categ');
      for(let itens of $loc[0]){
        let x = itens.dataset.categoria
        itens.onclick = function(){
          var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                document.querySelector(".box-product").innerHTML = this.responseText;
              }
        
            };
          xhttp.open("GET", "includes/main/product.php?categ="+x, true);
          xhttp.send();
        }
      }     
    });               
  }

  
  this.newsletter = function($info){
    document.addEventListener("DOMContentLoaded", ()=>{
      $loc[0] = document.querySelectorAll($info.form);

      var xhttp = new XMLHttpRequest();

      for(let form of $loc[0]){
          form.onsubmit = function(ev){
            ev.preventDefault();
            let url = "inscribe";
            document.querySelector("[name='inscribe']").disabled = true;
            let inputs = form.querySelectorAll("input")
            for(let input of inputs){
              if(input.value){
                url += "&"+input.name+"="+input.value;
                input.value = " "
              }
            }

            this.showPosition = (position)=>{
              document.querySelector('[name="coordenadas"]').value = "Latitude: " + position.coords.latitude +"; Longitude: " + position.coords.longitude  
            }
            
            if (navigator.geolocation){
              navigator.geolocation.getCurrentPosition(this.showPosition);
            }
            else{
              x.innerHTML="O seu navegador não suporta Geolocalização.";
            }

            setTimeout(()=>{
              url += "&"+document.querySelector('[name="coordenadas"]').name+"="+document.querySelector('[name="coordenadas"]').value

              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                  if(this.responseText == "sucess"){
                    
                  }else{
                    document.querySelector("[name='inscribe']").disabled = false;
                  }
                }
          
              };
              xhttp.open("POST", "./includes/newsletter/newsletter.php?", true);
              xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
              xhttp.send(url+"&terms=aceito");

            }, 1000)
          }
      }
    });               
  }
}

let cmd = new Ajax();

cmd.categoria();

cmd.newsletter({
  form: "[name='newsletter']"
});

export{
    Ajax
}