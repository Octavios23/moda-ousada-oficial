var Effects = function(){
    let $loc = new Array();


    this.showForm = function($coord){
        document.addEventListener("DOMContentLoaded", ()=>{
            document.querySelector($coord)
            .onclick = function(){
                if(document.querySelector('.rot-it-inverse')){
                    document.querySelector('#slt-btn').classList.remove('rot-it-inverse')
                    document.querySelector('#slt-btn').classList.add('rot-it');
                    document.querySelector('.container-hidde').classList.remove("form-hidde"); 
                    document.querySelector('.container-hidde').classList.add("form-hidde-reverse");
                }
                else{
                    document.querySelector('#slt-btn').classList.remove('rot-it')
                    document.querySelector('#slt-btn').classList.add('rot-it-inverse')
                    document.querySelector('.container-hidde').classList.remove("form-hidde-reverse");
                    document.querySelector('.container-hidde').classList.add("form-hidde");    
                }
            }
        });
    }


    this.animation = function($coord, $para, $ani, $all){
        let obj = new Array();

        //APARECER ITENS NO MOMENTO EM QUE PASSAR O MOUSE PELA SECTION
        obj['ani'] = document.querySelector($coord);

        document.addEventListener("DOMContentLoaded", ()=>{
            if($all == true){
                obj['para'] = $para.split(',')
                obj['event'] = $ani.split(',')
                obj['x'] = 0;
    
                obj['ani'].onmouseenter = function(){
                        if(this.dataset.status == "off"){
                            this.dataset.status = 'on';
                            for(let itens of obj['para']){  
                                document.querySelector('.animate-'+itens).classList.add(obj['event'][obj['x']++])
                            }
                        }
                    }
                obj['ani'].ontouchmove = function(){
                    if(this.dataset.status == "off"){
                        this.dataset.status = 'on';
                        for(let itens of obj['para']){  
                            document.querySelector('.animate-'+itens).classList.add(obj['event'][obj['x']++])
                        }
                    }
                }
            }else{
               
                obj['ani'].onmouseenter = function(){
                    if(this.dataset.status == "off"){
                        this.dataset.status = 'on';
                        document.querySelector('.animate-'+$para).classList.add($ani)
                    }
                }
                obj['ani'].ontouchmove = function(){
                    if(this.dataset.status == "off"){
                        this.dataset.status = 'on';
                        document.querySelector('.animate-1'+$para).classList.add($ani)
                    }
                }
            }   
        });
    }

    this.dropCart = function($drop){
        let $alt;

        $loc[0] = document.querySelectorAll($drop.coord);
        
        for(let itens of $loc[0]){
            itens.onclick = function(){

                let $scopo = document.querySelector($drop.coordBody)
                if($scopo.style.maxHeight){
                    itens.style.background = "#BD134C"
                    $scopo.style.maxHeight = null
                    $scopo.classList.remove("mt-2")
                }else{
                    itens.style.background = "#840731"
                    $scopo.style.maxHeight = $scopo.scrollHeight+"px";
                    $scopo.classList.add("mt-2")
                }
                
            }
        }
    }

    //ADMIN - NOT CONFIG
    this.opacity = function(){
        document.addEventListener("DOMContentLoaded", ()=>{
            function opacityAction0(x,y,z){
                if(x.dataset.status == 'off' ){
                    x.dataset.status = 'on';
                    document.querySelector(y).style="display:flex;transition: opacity "+z+"s;opacity: 1";
                }
            }
            
            function opacityAction1(x,y,z){
                if(x.dataset.status == 'on' ){
                    x.dataset.status = 'off';
                    document.querySelector(y).style="display:none;transition: opacity "+z+"s;opacity: 0;";
                }
            }
        });
    }

    this.toggleMenu = function($coord, $navbar){
        let btn = document.querySelector($coord);
        btn.onclick = function(){
            btn.classList.toggle("change");
    
            var y = document.querySelector($navbar);
            if(y.classList.contains("show-me") == false){
                y.classList.remove('no-show');
                y.classList.add('show-me');
            }
            else{
                y.classList.add('no-show');
                y.classList.remove('show-me');
            }
        }
    }
}

let cmd = new Effects();

if(document.querySelector('#slt-bt')){
    cmd.showForm('#slt-bt')
}

cmd.opacity();
if(document.querySelector("[data-status]")){  
    cmd.animation('[data-animation="one"]', 1, 'fade-in');
    cmd.animation('[data-animation="two"]', 2, 'fade-in');
    cmd.animation('[data-animation="three"]', 3, 'in-up');
    cmd.animation('[data-animation="four"]', '4,5,6,7', 'in-down,in-left,in-up,in-right',true);
    cmd.animation('[data-animation="five"]', '8,9', 'in-down,in-down',true);
    cmd.animation('[data-animation="six"]', '10,11,12,13,14', 'in-left,in-up,in-right,in-left,in-right',true);
}

cmd.dropCart({
    coord: ".box-header",
    coordBody: ".cart-content"
});


//Toggle Menu
if(document.querySelector(".js-btn-toggle")){
    cmd.toggleMenu('.js-btn-toggle', '.navbar');
}


export{
    Effects
}