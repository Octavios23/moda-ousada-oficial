
  
function handleFileSelect(evt) {
  loc = document.getElementById('list')
  loc.innerHTML = " "
  var files = evt.target.files; // Objeto FileList guarda todos os arquivos.

  //Intera sobre os arquivos e lista esses objetos no output.
  for (var i = 0, f; f = files[i]; i++) {
    if(i == 3){
      var div = document.querySelector('.showMore').innerHTML = '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="images" class="svg-inline--fa fa-images fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v48H54a6 6 0 0 0-6 6v244a6 6 0 0 0 6 6h372a6 6 0 0 0 6-6v-10h48zm42-336H150a6 6 0 0 0-6 6v244a6 6 0 0 0 6 6h372a6 6 0 0 0 6-6V86a6 6 0 0 0-6-6zm6-48c26.51 0 48 21.49 48 48v256c0 26.51-21.49 48-48 48H144c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h384zM264 144c0 22.091-17.909 40-40 40s-40-17.909-40-40 17.909-40 40-40 40 17.909 40 40zm-72 96l39.515-39.515c4.686-4.686 12.284-4.686 16.971 0L288 240l103.515-103.515c4.686-4.686 12.284-4.686 16.971 0L480 208v80H192v-48z"></path></svg>'

      if (f.type.match('image.*')) {
        var reader = new FileReader();
        //A leitura do arquivo é assíncrona 
        reader.onload = (function(theFile) {
          return function(e) {
            // console.log('Img info', e, theFile);
            // Gera a miniatura:
            var img = document.createElement('img');
            img.src = e.target.result;
            img.title = escape(theFile.name);
  
            var span = document.createElement('span');
            span.style = 'display: none;'
            
            span.appendChild(img);
            loc.insertBefore(span, null); //Local onde será inserido as imagens
          };
        })(f);
  
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
      }
    }
    else if(i > 3){
      // console.log('Objeto do arquivo', f);
      // Informação adicional se for imagem:
      if (f.type.match('image.*')) {
        var reader = new FileReader();
        //A leitura do arquivo é assíncrona 
        reader.onload = (function(theFile) {
          return function(e) {
            // console.log('Img info', e, theFile);
            // Gera a miniatura:
            var img = document.createElement('img');
            img.src = e.target.result;
            img.title = escape(theFile.name);
  
            var span = document.createElement('span');
            span.style = 'display: none;'
            
            span.appendChild(img);
            loc.insertBefore(span, null); //Local onde será inserido as imagens
          };
        })(f);
  
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
      }
    }
    else{
      // console.log('Objeto do arquivo', f);
      // Informação adicional se for imagem:
      if (f.type.match('image.*')) {
        var reader = new FileReader();
        //A leitura do arquivo é assíncrona 
        reader.onload = (function(theFile) {
          return function(e) {
            // console.log('Img info', e, theFile);
            // Gera a miniatura:
            var img = document.createElement('img');
            img.src = e.target.result;
            img.title = escape(theFile.name);
  
            var span = document.createElement('span');
            
            span.appendChild(img);
            loc.insertBefore(span, null); //Local onde será inserido as imagens
          };
        })(f);
  
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
      }
    }
  }
}
  
document.getElementById('files').addEventListener('change', handleFileSelect, false);
   
  
  
  