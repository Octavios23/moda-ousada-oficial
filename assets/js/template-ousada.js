import * as ousadaEffectsScript from './main/effects/_effects.js';
import * as ousadaAjaxScript from './main/Ajax/_ajax.js';
import * as ousadaCodeScript from './main/codes/_codes.js';
import * as ousadaMask from './main/mask/_mask.js';