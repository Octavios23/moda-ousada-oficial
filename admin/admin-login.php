<?php 

//if(!isset($_GET['4c3ss3:0n'])):
    //header('Location: https://modaousada.com');
//endif;


//CALL BD
include_once '../data/connect_db.php';

//HEAD DOC
require './admin-header.php';

?>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="background-video">
                    <div class="background-video-iframe">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/TwOjmxlwjDQ?autoplay=1&loop=1&mute=1&rel=0&showinfo=0&color=white&iv_load_policy=3&playlist=TwOjmxlwjDQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 ousada_param">
                                <div class="ousada_content_box">
                                    <div class="info-box">
                                        <div class="header-img">
                                            <img src="http://modaousada.com/img/logo.png" class="w-25" alt="Logotipo - Moda ousada">
                                        </div>
                                    </div>   
                                    <div class="info-box">
                                        <div class="header-txt">
                                            <h1 class="font-weight-bold" >AREA ADMIN</h1>
                                        </div>
                                    </div>
                                    
                                    <?php
                                        if(isset($_GET['recuperacao'])):
                                    ?>
                                        <!-- FORMULÁRIO DE RECUPERAÇÃO -->
                                    <form action="./data/system.php" method="POST">
                                        <div class="input-row">
                                            <div class="input-box">
                                                <label for="nome" class="font-weight-bold">NOME:</label>
                                                <input type="text" name="nome" class="form-control" required>
                                            </div>
                                            <div class="input-box">
                                                <label for="login" class="font-weight-bold">LOGIN:</label>
                                                <input type="text" name="login" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="input-row">
                                            <div class="input-box">
                                                <label for="tel" class="font-weight-bold">TELEFONE:</label>
                                                <input type="tel" name="tel" class="form-control" required>
                                            </div>
                                            <div class="input-box">
                                                <label for="email" class="font-weight-bold">EMAIL:</label>
                                                <input type="email" name="email" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="button-box">
                                            <button class="btn btn-secondary" type="submit" name="btn-recover">RECUPERAR</button>
                                        </div>
                                    </form>
                                    <div class="forget">
                                        <div class="forget_box">
                                            <div class="forget_box-fields">
                                                <p>Volte para a área de login clicando <a href="./admin-login.php">aqui</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        else:
                                    ?>
                                    
                                    <!-- FORMULÁRIO DE VALIDAÇÃO -->
                                    <form action="./data/system.php" method="POST">
                                        <div class="input-box ">
                                            <label for="login" class="font-weight-bold">LOGIN</label>
                                            <input type="text" name="login" id="login" class="form-control" required>
                                        </div>
                                        <div class="input-box ">
                                            <label for="password" class="font-weight-bold">PASSWORD</label>
                                            <input type="password" name="password" id="password" class="form-control" required>
                                        </div>
                                        <div class="button-box">
                                            <button class="btn btn-secondary" type="submit" name="btn-login">ENTRAR</button>
                                        </div>
                                    </form>
                                    <div class="forget">
                                        <div class="forget_box">
                                            <div class="forget_box-fields">
                                                <p>Esqueceu a sua senha? Recupere clicando <a href="./admin-login.php?recuperacao">aqui</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php 
                                        endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<script>
    //Envia um alerta de mensagem se der algo errado
    if('<?php echo $_SESSION['mensagem']?>' != ''){
        alert('<?php echo $_SESSION['mensagem']?>')
        <?php echo $_SESSION['mensagem'] = ''?>
    }
</script>

<?php 

//FOOTER DOC 
include_once './admin-footer.php';

?>