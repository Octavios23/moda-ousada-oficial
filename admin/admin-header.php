<?php 
 $uri_bool = true;
 $urls = explode("/", $_SERVER["REQUEST_URI"]);

 foreach($urls as $url){
     if($url == "admin-login.php" || $url == "admin-login"){
         $uri_bool = false;
     }
 }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moda Ousada - Store</title>

    <link rel="shortcut icon" href="https://modaousada.com/img/icone.png" >

    <meta name="description" content=" Loja Virtual de Artigos Íntimos Femininos e moda feminina. Maricá, RJ - Brasil. Acesse já">
    <meta name="robots" content="index">
    <meta name="author" content="#ArtigosíntimosFemininos">
    <meta name="keywords" content="Moda íntima, sutiã, calcinha, lingerie, feminino">

    <meta property="og:type" content="page">
    <meta property="og:url" content="https://instagram.com/ousadamodaintima98?igshid=1qxor8nsnjxmv">
    <meta property="og:title" content="Moda Ousada">
    <meta property="og:image" content="https://modaousada.com/img/icone.png">
    <meta property="og:description" content="Loja Virtual de Artigos Íntimos Femininos e moda feminina. Maricá, RJ - Brasil. Acesse já">

    <meta property="article:author" content="Moda Ousada">


    <!-- Folhas de Estilo -->
    <?php  if($uri_bool == true){ ?>
    <link rel="stylesheet" href="../assets/css/libary/bootStrap/bootstrap.min.css">
    <?php }?>
    <link rel="stylesheet" href="./assets/css/admin-ousada.css">
              

</head>
<body onunload="window.opener.location.reload();">
<div class="content">
    <?php 

        if($uri_bool == true){
    ?>
    <header>
        <div class="container-fluid ousada">
            <div class="row ousada_development">
                <div class="col-12 ousada_development-collum px-0">
                    <div class="ousada_development-box">
                        <div class="ousada_development-bloc">
                            <div class="ousada_development-bloc-logo">
                                <img src="./../assets/img/logo.png" alt="Logotipo da Ousada - Moda íntima">
                            </div>
                        </div>
                        <div class="ousada_development-bloc">
                            <div class="ousada_development-bloc-sidebar" data-close-sidebar>
                                <div class="ousada_development-bloc-sidebar-toggle">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
                                </div>
                            </div>
                        </div>  
                        <div class="ousada_development-bloc">
                            <div class="ousada_development-bloc-search">
                                <div class="ousada_development-bloc-search-input">
                                    <input type="text" name="filtro" id="filtro-nome">
                                </div>
                            </div>
                        </div>
                        <div class="ousada_development-bloc">
                            <div class="ousada_development-bloc-notify">
                                <div class="ousada_development-bloc-notify-icon">
                                    <span data-notify></span>
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="bell" class="svg-inline--fa fa-bell fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M439.39 362.29c-19.32-20.76-55.47-51.99-55.47-154.29 0-77.7-54.48-139.9-127.94-155.16V32c0-17.67-14.32-32-31.98-32s-31.98 14.33-31.98 32v20.84C118.56 68.1 64.08 130.3 64.08 208c0 102.3-36.15 133.53-55.47 154.29-6 6.45-8.66 14.16-8.61 21.71.11 16.4 12.98 32 32.1 32h383.8c19.12 0 32-15.6 32.1-32 .05-7.55-2.61-15.27-8.61-21.71zM67.53 368c21.22-27.97 44.42-74.33 44.53-159.42 0-.2-.06-.38-.06-.58 0-61.86 50.14-112 112-112s112 50.14 112 112c0 .2-.06.38-.06.58.11 85.1 23.31 131.46 44.53 159.42H67.53zM224 512c35.32 0 63.97-28.65 63.97-64H160.03c0 35.35 28.65 64 63.97 64z"></path></svg>
                                </div>
                            </div>
                            <div class="ousada_development-bloc-mob">
                                <div class="ousada_development-bloc-mob-sidebar" data-close-mob-sidebar>
                                    <div class="ousada_development-bloc-mob-sidebar-toggle">
                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <sidebar>
        <div class="container-fluid ousada content-sidebar">
            <div class="row ousada_development">
                <div class="col-12 ousada_development-collum">
                    <div class="ousada_development-bloc">
                        <div class="ousada_development-bloc-sidebar" data-open-sidebar>
                            <div class="ousada_development-bloc-sidebar-toggle">
                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" class="svg-inline--fa fa-bars fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"></path></svg>
                            </div>
                        </div>
                    </div>
                    <div class="ousada_development-navbar">
                        <nav>
                            <ul>
                                <li>
                                    <a class="sidebar-active-menu" data-menu="./parts/dashboard/product-area.php"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="boxes" class="svg-inline--fa fa-boxes fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M560 288h-80v96l-32-21.3-32 21.3v-96h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16zm-384-64h224c8.8 0 16-7.2 16-16V16c0-8.8-7.2-16-16-16h-80v96l-32-21.3L256 96V0h-80c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16zm64 64h-80v96l-32-21.3L96 384v-96H16c-8.8 0-16 7.2-16 16v192c0 8.8 7.2 16 16 16h224c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16z"></path></svg> <span>Produtos</span></a>
                                </li>
                                <li>
                                    <a data-menu="./parts/dashboard/cliente-area.php"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user-tag" class="svg-inline--fa fa-user-tag fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M630.6 364.9l-90.3-90.2c-12-12-28.3-18.7-45.3-18.7h-79.3c-17.7 0-32 14.3-32 32v79.2c0 17 6.7 33.2 18.7 45.2l90.3 90.2c12.5 12.5 32.8 12.5 45.3 0l92.5-92.5c12.6-12.5 12.6-32.7.1-45.2zm-182.8-21c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24c0 13.2-10.7 24-24 24zm-223.8-88c70.7 0 128-57.3 128-128C352 57.3 294.7 0 224 0S96 57.3 96 128c0 70.6 57.3 127.9 128 127.9zm127.8 111.2V294c-12.2-3.6-24.9-6.2-38.2-6.2h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 287.9 0 348.1 0 422.3v41.6c0 26.5 21.5 48 48 48h352c15.5 0 29.1-7.5 37.9-18.9l-58-58c-18.1-18.1-28.1-42.2-28.1-67.9z"></path></svg> <span>Clientes</span></a>
                                </li>
                                <li>
                                    <a data-menu="./parts/dashboard/information-area.php"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="id-badge" class="svg-inline--fa fa-id-badge fa-w-12" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M336 0H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V48c0-26.5-21.5-48-48-48zM144 32h96c8.8 0 16 7.2 16 16s-7.2 16-16 16h-96c-8.8 0-16-7.2-16-16s7.2-16 16-16zm48 128c35.3 0 64 28.7 64 64s-28.7 64-64 64-64-28.7-64-64 28.7-64 64-64zm112 236.8c0 10.6-10 19.2-22.4 19.2H102.4C90 416 80 407.4 80 396.8v-19.2c0-31.8 30.1-57.6 67.2-57.6h5c12.3 5.1 25.7 8 39.8 8s27.6-2.9 39.8-8h5c37.1 0 67.2 25.8 67.2 57.6v19.2z"></path></svg><span>Informações Pessoais</span></a>
                                </li>
                                <li>
                                    <a href="./data/system.php?logout"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="door-open" class="svg-inline--fa fa-door-open fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M624 448h-80V113.45C544 86.19 522.47 64 496 64H384v64h96v384h144c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM312.24 1.01l-192 49.74C105.99 54.44 96 67.7 96 82.92V448H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h336V33.18c0-21.58-19.56-37.41-39.76-32.17zM264 288c-13.25 0-24-14.33-24-32s10.75-32 24-32 24 14.33 24 32-10.75 32-24 32z"></path></svg><span>Sair</span></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </sidebar>
    <?php }?>
