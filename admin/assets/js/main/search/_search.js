let Search = function(){
    
    this.table = function($info){
        let $filtro = document.querySelector($info.element)
        let $table = document.querySelector($info.table)

        $filtro.onkeyup = ()=>{
            var child = $filtro.value.toLowerCase();
            for(let x =1; x < $table.rows.length; x++){
                let value_cel = $table.rows[x].cells[1].innerText;
                value_cel = value_cel.toLowerCase().indexOf(child) >= 0;
                $table.rows[x].style.display = value_cel ? '': 'none';
            }
        }
    }

    this.bloc = function($info){
        let $filtro = document.querySelector($info.element)
        let $bloc = document.querySelectorAll($info.bloc)

        $filtro.onkeyup = ()=>{
            var value = $filtro.value.toLowerCase();
            for(let bloc of $bloc){
                let value_cel = bloc.querySelector($info.child).innerText;
                value_cel = value_cel.toLowerCase().indexOf(value) >= 0;
                bloc.style.display = value_cel ? '': 'none';
            }
        }
    }

    this.btn = function($info){
        let $btn = document.querySelector($info.btn);
        let $bloc = document.querySelectorAll($info.bloc);
        let $table = document.querySelector($info.table);

        $btn.onclick = ()=>{
            var value = $info.filtro.toLowerCase();
            if($info.type == false){
                for(let bloc of $bloc){
                    let value_cel = bloc.querySelector($info.child).innerText;
                    value_cel = value_cel.toLowerCase().indexOf(value) == 0;
                    bloc.style.display = value_cel ? '': 'none';
                }
            }else{
                for(let x =1; x < $table.rows.length; x++){
                    let value_cel = $table.rows[x].querySelector($info.child).innerText;
                    value_cel = value_cel.toLowerCase().indexOf(value) == 0;
                    $table.rows[x].style.display = value_cel ? '': 'none';
                }
            }
            
        }
    }
}



export{
    Search
}