const Info = function(){
    this.radios = document.querySelectorAll("[name='id']");
    this.btn_edit = document.querySelector(".ousada_actions-btn-icon-edit")


    this.search_id = ()=>{
        for(let radio of this.radios){
            radio.onclick = ()=>{
                this.btn_edit.href = "./parts/admin-edit.php?id="+radio.value
            }
        }
    }


    this.open_Window_Edit = ()=>{
        this.search_id();

        this.btn_edit.onclick = function(ev){
            ev.preventDefault();
            if(this.href.indexOf("?") > 0){
                this.target = "_blank"
                window.open(this.href,'pop','width=500,height=800'); 
            }else{
                alert("Escolha um Produto para ser editado!")
            }
        }
    }

}

let cmd = new Info();

cmd.open_Window_Edit();


