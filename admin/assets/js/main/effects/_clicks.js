
let Clicks = function(){

    this.windowGo = function($coord){
        window.scrollTo($coord.x, $coord.y); //Aloca o usuário para o topo da janela
    }

    this.sidebar = function($coord){
        document.querySelector($coord.open).onclick = function(){
            document.querySelector($coord.local).classList.toggle('sidebar-active')
            document.querySelector($coord.sidebar).classList.toggle('sidebar-active')
            document.querySelector($coord.others).classList.toggle('sidebar-active')
        }
        document.querySelector($coord.close).onclick = function(){
            document.querySelector($coord.local).classList.toggle('sidebar-active')
            document.querySelector($coord.sidebar).classList.toggle('sidebar-active')
            document.querySelector($coord.others).classList.toggle('sidebar-active')
        }
    }

    this.sidebar_mob = function($coord){
        document.querySelector($coord.btn).onclick = function(){
            document.querySelector($coord.local).classList.toggle('sidebar-mob-active')
        }
        window.onscroll = function(){
            if(document.querySelector('.sidebar-mob-active')){
                
                document.querySelector($coord.local).classList.remove('sidebar-mob-active')
            }
        }
    }

}

let cmd = new Clicks();

// if(document.querySelector('.lrm_content-effect-item')){
//     document.querySelector('.lrm_content-effect-item').onclick = ()=>{
//         cmd.windowGo({x: 0, y: 0})
//     }
// }

document.addEventListener('DOMContentLoaded', ()=>{
    if(document.querySelector('sidebar') && window.innerWidth > 800){
        cmd.sidebar({
            local: '.content-info',
            sidebar: 'sidebar',
            others: 'header',
            open: '[data-open-sidebar]',
            close: '[data-close-sidebar]'
        })
    }
})

document.addEventListener('DOMContentLoaded', ()=>{
    if(document.querySelector('[data-close-mob-sidebar]') && window.innerWidth < 800){
        cmd.sidebar_mob({
            local: 'sidebar',
            btn: '[data-close-mob-sidebar]'
        })
    }
})



export{
    Clicks
}