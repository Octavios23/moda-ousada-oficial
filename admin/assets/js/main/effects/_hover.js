let Hover = function(){

    
    this.preview_img = function($coord){
        document.getElementById($coord.files).onchange = function(evt) {
            let loc = document.getElementById($coord.list)
            loc.innerHTML = " "
            var files = evt.target.files; // Objeto FileList guarda todos os arquivos.
          
            //Intera sobre os arquivos e lista esses objetos no output.
            for (var i = 0, f; f = files[i]; i++) {

                if (f.type.match('image.*')) {
                  var reader = new FileReader();
                  //A leitura do arquivo é assíncrona 
                  reader.onload = (function(theFile) {
                    return function(e) {
                      // console.log('Img info', e, theFile);
                      // Gera a miniatura:
                      var img = document.createElement('img');
                      img.src = e.target.result;
                      img.title = escape(theFile.name);
            
                      var span = document.createElement('span');
                      
                      span.appendChild(img);
                      loc.insertBefore(span, null); //Local onde será inserido as imagens
                    };
                  })(f);
            
                  // Read in the image file as a data URL.
                  reader.readAsDataURL(f);
                }
              
            }
          }
            
               
    }
}

let cmd = new Hover();


document.addEventListener("DOMContentLoaded", ()=>{
    cmd.preview_img({
        files: "files",
        list: "list"
    })
})


export{
    Hover
}