let Ajax = function(){
 
  this.menu = function($info){
    document.addEventListener("DOMContentLoaded", ()=>{
      let btns = document.querySelectorAll($info.btns)

      for(let btn of btns){
        btn.onclick = function(){
          let url = btn.dataset.menu

          for(let itens of btns){
            itens.classList.remove("sidebar-active-menu")
          }
          document.querySelector('sidebar').classList.remove('sidebar-mob-active')

          this.classList.add("sidebar-active-menu")
          var xhttp = new XMLHttpRequest();
  
          xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              document.querySelector($info.content).innerHTML = this.responseText;
            }
      
          };
  
          xhttp.open("GET", url+"?page", true);
          xhttp.send();
        }   
      } 
    });
  }     
  
  this.save_info = function($info){

    document.querySelector("[name='save-info']").onclick = ()=>{
      document.querySelector($info.form).onsubmit = (e) =>{
        e.preventDefault();
        
        let name = document.querySelector("[name='nome']").value
        let user = document.querySelector("[name='user']").value
        let email = document.querySelector("[name='email']").value
        let codigo = document.querySelector("[name='codigo']").value
        let senha = document.querySelector("[name='senha']").value
        let telefone = document.querySelector("[name='telefone']").value
  
        var xhttp = new XMLHttpRequest();
    
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            alert(this.responseText);
          }
        };
    
        xhttp.open("GET", "./data/system.php?save-info&nome="+name+"&user="+user+"&email="+email+"&codigo="+codigo+"&senha="+senha+"&telefone="+telefone, true);
        xhttp.send();
      }
    }
  }

}



export{
    Ajax
}