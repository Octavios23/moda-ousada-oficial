import * as Click from "./main/effects/_clicks.js";
import * as Hover from './main/effects/_hover.js';
import * as search from "./main/search/_search";
import * as ajax from "./main/Ajax/_ajax.js";
import * as Info from './main/info/_info.js';

let src = new search.Search()
let cmd = new ajax.Ajax();


// CRIANDO OS BOTÕES DE MENU DINÂMICOS

cmd.menu({
    btns: "[data-menu]",
    content: ".content-main",
})



/**
 ** FILTRO DE PRODUTOS 
 * 
 * @param element  Parâmetro do método "table", de origem do arquivo search. Nele voce defini a input para filtrar os campos.
 * @param table  Parâmetro para definir a localização da tabela onde são expostos os valores do produtos.
 */

setInterval(()=>{
    if(document.querySelector(".product-area")){
        if(window.innerWidth > 600){
            src.table({
                element: "#filtro-nome",
                table: "#lista"
            })
        
            src.btn({
                type: true,
                table: "#lista",
                btn: '[data-search="disponibilidade"]',
                child: "[data-disponibilidade]",
                filtro: "disponível"
            })
            src.btn({
                type: true,
                table: "#lista",
                btn: '[data-search="privacidade"]',
                child: "[data-privacidade]",
                filtro: "privado"
            })
            src.btn({
                type: true,
                table: "#lista",
                btn: '[data-search="todos"]',
                child: "[data-name]",
                filtro: ""
            })
        
        }else{
            src.bloc({
                element: "#filtro-nome",
                bloc: ".ousada_development-mob-list li",
                child: ".ousada_development-mob-list h4"
            })
            src.btn({
                table: false,
                btn: '[data-search="disponibilidade"]',
                bloc: ".ousada_development-mob-list li",
                child: "[data-disponibilidade]",
                filtro: "disponível"
            })
            src.btn({
                table: false,
                btn: '[data-search="privacidade"]',
                bloc: ".ousada_development-mob-list li",
                child: "[data-privacidade]",
                filtro: "privado"
            })
            src.btn({
                table: false,
                btn: '[data-search="todos"]',
                bloc: ".ousada_development-mob-list li",
                child: ".ousada_development-mob-list h4 strong",
                filtro: ""
            })
        }
        
    }
}, 1000)

setInterval(()=>{
    if(document.querySelector(".cliente-area")){
        if(window.innerWidth > 600){
            src.table({
                element: "#filtro-nome",
                table: "#lista"
            })
        }else{
            src.bloc({
                element: "#filtro-nome",
                bloc: ".ousada_development-mob-list li",
                child: ".ousada_development-mob-list h4 strong"
            })
        
        }
    }
}, 1000)

setInterval(() => {
    if(document.querySelector("[name='save-info']")){        
        cmd.save_info({
            form: "[name='informations']"
        })
    }
}, 1000);