<?php 
       $uri_bool = true;
       $urls = explode("/", $_SERVER["REQUEST_URI"]);
      
       foreach($urls as $url){
           if($url == "admin-login.php" || $url == "admin-login"){
               $uri_bool = false;
           }
       }

        if($uri_bool == true){
    ?>
 <footer>
     <div class="container-fluid">
         <div class="row">
             <div class="col-12">
                 <div class="container">
                     <div class="row">
                         <div class="col-12">
                            <div class="ousada">
                                <div class="ousada-footer">
                                    <div class="ousada-footer-info text-center">
                                        <p>@2021 All Rights Reserved Modaousada - Desenvolved Company Market</p>
                                    </div>
                                </div>
                            </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </footer>
 </div>
<?php }else{ ?>
    </div> <?php } ?>
<?php 

if($uri_bool == true){
?>

    <!-- MODAL DE CRIAÇÃO PRODUTO -->
    <div class="modal fade" id="CreateProduct" tabindex="-1" role="dialog" aria-labelledby="CreateProductTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="./data/system.php" method="POST" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="CreateProductTitle">Criação de Produtos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-box">
                            <label for="name">Nome</label>
                            <div class="form-input">
                                <input type="text" name="name" placeholder="Ex: Bruna.." required="true">
                            </div>
                        </div>
                        <div class="check-box my-4" style="text-align: center;">
                            <label for="tamanho">Tamanhos</label>
                            <div class="forms-check">
                                <div class="form-check">
                                    <span>P</span>
                                    <input type="checkbox" name="tamanho[]"  value="Pequeno" >
                                </div>
                                <div class="form-check">
                                    <span>M</span>
                                    <input type="checkbox" name="tamanho[]"  value="Medio"> 
                                </div>
                                <div class="form-check">
                                    <span>G</span>
                                    <input type="checkbox" name="tamanho[]"  value="Grande">
                                </div>
                                <div class="form-check">
                                    <span>GG</span>
                                    <input type="checkbox" name="tamanho[]"  value="Extra Grande" >
                                </div>
                                <div class="form-check">
                                    <span>XG</span>
                                    <input type="checkbox" name="tamanho[]"  value="Extra x Grande">
                                </div> 
                            </div>
                        </div>
                        <div class="check-box my-4 form-categ" style="text-align: center;">
                            <label for="categoria">Categorias</label>
                            <div class="forms-check">
                                <div class="form-check">
                                    <span>O.M.V<br><strong>(Os Mais Vendidos)</strong></span>
                                    <input type="checkbox" class="d-block" name="categoria[]" id="OsMaisVendidos"  value="Os Mais Vendidos">
                                </div>
                                <div class="form-check">
                                    <span>N.V<br><strong>(Nossas Novidades)</strong></span>
                                    <input type="checkbox" class="d-block" name="categoria[]" id="NossasNovidades" value="Nossas Novidades"> 
                                </div>
                                <div class="form-check">
                                    <span>N.P<br><strong>(Nossas Promoções)</strong></span>
                                    <input type="checkbox" class="d-block" name="categoria[]" id="NossasPromoções" value="Nossas Promoções">
                                </div>
                            </div>
                        </div>
                        <div class="input-box">
                            <label for="price">Preço</label>
                            <div class="form-input">
                                <input type="text" name="price" placeholder="Ex: 20" required="true">
                            </div>
                        </div>
                        <div class="check-box my-4" style="text-align: center;">
                            <label for="categoria">Disponibilidade</label>
                            <div class="forms-check">
                                <div class="form-box">
                                    <label>Sim</label>
                                    <input type="checkbox" name="disponibilidade"  value="1"> 
                                </div>
                                <div class="form-box">
                                    <label>Não</label>
                                    <input type="checkbox" name="disponibilidade" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="check-box my-4" style="text-align: center;">
                            <label for="categoria">Privacidade</label>
                            <div class="forms-check">
                                <div class="form-box">
                                    <label>Sim</label>
                                    <input type="checkbox" name="privacidade"  value="1">
                                </div>
                                <div class="form-box">
                                    <label>Não</label>
                                    <input type="checkbox" name="privacidade" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="input-box">
                            <label for="arquivo">img</label>
                            <input id="files" type="file" name="arquivo[]" accept="image/*" style="overflow: hidden;" multiple required="true">
                            <div class="preview" style="display: flex;">
                                <output id="list"></output>
                                <div class="showMore" onmouseenter="statusOutPut(this, 1)" onmouseleave="statusOutPut(this, 2)"  data-response="off"></div>
                                <div class="box-preview" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <div class="btn-div" >
                            <button class="btn btn-danger" type="submit" name="create">Adicionar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php }?>

    <!-- Libarys -->
    <script src="../assets/js/libarys/jQuery/jquery-3.6.0.min.js"></script>
    <script src="../assets/js/libarys/bootStrap/bootstrap.min.js"></script>
    <script src="./assets/js/libary/swiper/swiper-bundle.min.js"></script>
    
    <!-- Actions -->
    <script type="module" src="./assets/js/admin-ousada.js"></script>
    <script type="module" src="./assets/js/main/codes/_codes.js"></script>


</body>
</html>