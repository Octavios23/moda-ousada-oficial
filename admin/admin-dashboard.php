
<?php 
session_start();
//Verificação -- Para que nenhum fdp entre pela url;
if(!isset($_SESSION['logado'])):
    header('Location: http://localhost/igear/modaousa/admin/admin-login.php');
endif;


require './admin-header.php';
require '../data/connect_db.php';

?>


<div class="content-info">
    <section class="content-main" >
        <?php 
        
        require './parts/dashboard/product-area.php';
        
        ?>
    </section>


<?php 

require './admin-footer.php';

?>