<?php 
//SESSION
session_start();

//CONEXÃO 
require '../../data/connect_db.php';


//Select 
if(isset($_GET['id'])): // USOU O GET PORQUE A VERIFICAÇÃO É PELA URL
    $id = ucfirst(htmlspecialchars(ucwords( $_GET['id']))); // Filtrou o id do banco de dados.

    $_SESSION['id'] = $id;

    try{
        $consulta = $pdo->query("SELECT * FROM ig_produtos WHERE  id_produto = $id "); // Selecionou todos os campos da tabela com o mesmo id que a variável;
        $dados = $consulta->fetch(PDO::FETCH_ASSOC);
    }catch(PDOException $e){
        echo "Ocorreu algum erro";
    }
endif;

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editor de Produtos</title>

    <link rel="stylesheet" href="../../assets/css/libary/bootStrap/bootstrap.min.css">
    <link rel="stylesheet" href="./../assets/css/admin-ousada.css">
</head>
<body>
    <div class="container editor">
        <div class="row">
            <div class="col-12">
                <div class="title my-4">
                    <h2>Editor de Produtos</h2>
                </div>
                <form action="../data/system.php" method="POST" enctype="multipart/form-data">
                    <div class="input-box">
                        <input type="hidden" name="id" value="<?php echo $dados['id_produtos']?>">
                    </div>
                    <div class="input-box">
                        <label for="name">Nome</label>
                        <div class="form-input">
                            <input type="text" name="name" placeholder="Ex: Bruna.." required="true" value="<?php echo $dados['nome']?>">
                        </div>
                    </div>
                    <div class="check-box my-4" style="text-align: center;">
                        <label for="tamanho">Tamanhos</label>

                        <?php 
                            $P = '';$M = ''; $G = ''; $GG =''; $XG ='';
                            //$archive = explode(",", $dados['categoria']);

                            if(stristr($dados['tamanho'],'Pequeno')){
                                $P = 'checked';
                            }
                            if(stristr($dados['tamanho'],'Medio')){
                                $M = 'checked';
                            }
                            if(stristr($dados['tamanho'],'Grande')){
                                $G = 'checked';
                            }
                            if(stristr($dados['tamanho'],'Extra Grande')){
                                $GG = 'checked';
                            }
                            if(stristr($dados['tamanho'],'Extra x Grande')){
                                $XG = 'checked';
                            }
                        ?>

                        <div class="forms-check">
                            <div class="form-check">
                                <span>P</span>
                                <input type="checkbox" name="tamanho[]"  value="Pequeno"  <?php echo $P?>>
                            </div>
                            <div class="form-check">
                                <span>M</span>
                                <input type="checkbox" name="tamanho[]"  value="Medio" <?php echo $M?>> 
                            </div>
                            <div class="form-check">
                                <span>G</span>
                                <input type="checkbox" name="tamanho[]"  value="Grande" <?php echo $G?>>
                            </div>
                            <div class="form-check">
                                <span>GG</span>
                                <input type="checkbox" name="tamanho[]"  value="Extra Grande" <?php echo $GG?>>
                            </div>
                            <div class="form-check">
                                <span>XG</span>
                                <input type="checkbox" name="tamanho[]"  value="Extra x Grande" <?php echo $XG?>>
                            </div> 
                        </div>
                    </div>
                    <div class="check-box my-4 form-categ" style="text-align: center;">
                        <label for="categoria">Categorias</label>
                        <?php 
                            $OMV = '';$NN = ''; $NP = '';
                            //$archive = explode(",", $dados['categoria']);

                            if(stristr($dados['categoria'],'Os Mais vendidos')){
                                $OMV = 'checked';
                            }
                            if(stristr($dados['categoria'],'Nossas Novidades')){
                                $NN = 'checked';
                            }
                            if(stristr($dados['categoria'],'Nossas Promoções')){
                                $NP = 'checked';
                            }
                        ?>
                        <div class="forms-check">
                            <div class="form-check">
                                <span>O.M.V<br><strong>(Os Mais Vendidos)</strong></span>
                                <input type="checkbox" class="d-block" name="categoria[]" id="OsMaisVendidos"  value="Os Mais Vendidos" <?php echo $OMV?>>
                            </div>
                            <div class="form-check">
                                <span>N.V<br><strong>(Nossas Novidades)</strong></span>
                                <input type="checkbox" class="d-block" name="categoria[]" id="NossasNovidades" value="Nossas Novidades" <?php echo $NN?>> 
                            </div>
                            <div class="form-check">
                                <span>N.P<br><strong>(Nossas Promoções)</strong></span>
                                <input type="checkbox" class="d-block" name="categoria[]" id="NossasPromoções" value="Nossas Promoções" <?php echo $NP?>>
                            </div>
                        </div>
                    </div>
                    <div class="input-box">
                        <label for="price">Preço</label>
                        <div class="form-input">
                            <input type="text" name="price" placeholder="Ex: 20" required="true"  value="<?php echo $dados['preco']?>">
                        </div>
                    </div>
                    <div class="check-box my-4" style="text-align: center;">
                        <label for="categoria">Disponibilidade</label>
                            <?php
                                if($dados['disponibilidade'] == 1){
                                    $s_disponibilidade = "checked";
                                }else{
                                    $n_disponibilidade = "checked";
                                }
                            ?>
                        <div class="forms-check">
                            <div class="form-box">
                                <label>Sim</label>
                                <input type="checkbox" name="disponibilidade"  value="1" <?php echo  $s_disponibilidade?>> 
                            </div>
                            <div class="form-box">
                                <label>Não</label>
                                <input type="checkbox" name="disponibilidade" value="0" <?php echo  $n_disponibilidade?>>
                            </div>
                        </div>
                    </div>
                    <div class="check-box my-4" style="text-align: center;">
                        <label for="categoria">Privacidade</label>
                        <?php
                            if($dados['privacidade'] == 1){
                                $s_privacidade = "checked";
                            }else{
                                $n_privacidade = "checked";
                            }
                        ?>
                        <div class="forms-check">
                            <div class="form-box">
                                <label>Sim</label>
                                <input type="checkbox" name="privacidade"  value="1"  <?php echo $s_privacidade?>>
                            </div>
                            <div class="form-box">
                                <label>Não</label>
                                <input type="checkbox" name="privacidade" value="0" <?php echo  $n_privacidade?>>
                            </div>
                        </div>
                    </div>
                    <div class="input-box form-edit">
                        <label for="img">img</label>
                        <input id="files" type="file" name="arquivo[]" accept="image/*" style="overflow: hidden;" multiple>
                        <input type="checkbox" name='delIMG'> <span>Deseja excluir todas as imagens anteriores?</span>
                        <div class="imgBD">
                            <?php

                                $url = explode(';', $dados['img']);
                                
                                if(count($url) > 0): #Se a estrutura '$result' possuir linhas de parâmtros maiores que 0, vai ser feito:
                                    for($x = 0 ;$x < count($url); $x++): #Enquanto ainda houver valores no array(linhas) na variável 'result' ele vai produzir a estrutura abaixo;
                                        if($url[$x] != ''){
                                            echo '<img src="../'.$url[$x].'" alt="img'.$x.'" >';
                                        }
                                    endfor;
                                endif;
                                
                            ?>
                        </div>
                    </div>
                    <div class="box-button">
                        <button type="submit" name="editar" class="btn">Atualizar</button>
                        <a href="#"  class="btn green" onclick="closeDown()">Voltar ao Início</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
    
        function closeDown(){
            window.opener = window
            window.close("#")
        }
        opener.location.reload();
        
    </script>
       

</body>
</html>
