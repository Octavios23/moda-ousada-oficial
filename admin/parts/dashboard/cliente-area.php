<?php 
    if(isset($_GET['page'])){
        require '../../../data/connect_db.php';
    }
    $consulta = $pdo->query("SELECT * FROM ig_clientes ");
?>

<div class="cliente-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="ousada_development-status mb-4">
                                <div class="ousada_development-qtd">
                                    <div class="ousada_development-qtd-text">
                                        <span><?php echo $consulta ->rowCount()?> &nbsp; Clientes Cadastrados</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ousada_development-table">
                                <table id="lista">
                                    <thead>
                                        <tr>
                                            <th>Contatar</th>
                                            <th>Email</th>
                                            <th>Celular</th>
                                            <th>Localização</th>
                                            <th>Data de Registro</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            while ($field = $consulta->fetch(PDO::FETCH_ASSOC)) {?>
                                        <tr>
                                            <td>
                                                <a href="mailto: <?php echo $field['email'] ?>">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="paper-plane" class="svg-inline--fa fa-paper-plane fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path></svg>
                                                </a>
                                            </td>
                                            <td>
                                                <div class="ousada_table-block">
                                                    <div class="ousada_table-box-info">
                                                        <div class="ousada_table-box-info-email">
                                                            <h5><strong data-email><?php echo $field['email']?></strong></h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="https://api.whatsapp.com/send?phone=<?php echo $field['celular']?>&text=Bem+Vinda+a+Moda+Ousada!" target="_blank"><span style="color: black"><?php echo $field['celular']?></span></a>
                                            </td>
                                            <td>
                                                <span data-localizacao><?php echo $field['localizacao']?></span>
                                            </td>
                                            <td>
                                                <span><?php
                                                 $date =  explode(' ', $field['data_register']);
                                                 $struct = explode('-', $date[0]);
                                                 echo $struct[2]."/".$struct[1]."/".$struct[0]." - ".$date[1];?>
                                                </span>
                                            </td>
                                            <td>
                                                <a href="./data/system.php?delete&id=<?php echo $field['id_produto'] ?>">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt 
                                                    fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 
                                                    48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 
                                                    16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 
                                                    48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path></svg>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="ousada_development-mob">
                                <div class="ousada_development-mob-list">
                                    <ul>
                                        <?php 
                                        $consulta = $pdo->query("SELECT * FROM ig_clientes ");
                                            while ($field = $consulta->fetch(PDO::FETCH_ASSOC)) {?>
                                        <li>
                                            <div class="list--mob-box">
                                                <div class="list--mob-box-infos">
                                                    <div class="list--mob-box-infos-name">
                                                        <h4><?php echo $field['nome']?></h4>
                                                    </div>
                                                    <div class="list--mob-box-infos-describe">
                                                        <p><Strong data-email>Email:</Strong> <?php echo $field['email']?> <br> <strong>Celular:</strong> <?php echo $field['celular']?> <br> <strong>Localização:</strong> <span data-localizacao><?php echo $field['localizacao']  ?></span> <br><br> <strong>Data de Registro:</strong> <?php echo $field['data_register']?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="list--mob-select">
                                                <div class="list--mob-select-icon">
                                                    <a href="mailto: <?php echo $field['email'] ?>">
                                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="paper-plane" class="svg-inline--fa fa-paper-plane fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path></svg>
                                                    </a>
                                                </div>
                                                <div class="list--mob-select-icon">
                                                    <a href="mailto: <?php echo $field['email'] ?>">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="whatsapp" class="svg-inline--fa fa-whatsapp fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"></path></svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>