<?php 

    session_start();
    
    if(isset($_GET['page'])){
        require '../../../data/connect_db.php';
    }
    $consulta = $pdo->query("SELECT * FROM ig_adminstrador WHERE user = '".$_SESSION['user']."'");
    $field = $consulta->fetch(PDO::FETCH_ASSOC);
?>

<div class="information-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="ousada_development-title">
                                <h2><u>INFORMAÇÕES PESSOAIS</u></h2>
                            </div>
                            <form name="informations">
                                <div class="form-row">
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Nome</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['nome'] ?>" name="nome" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>E-mail</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['email'] ?>" name="email" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Telefone</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['telefone'] ?>" name="telefone" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Usuário</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['user'] ?>" name="user" class="form-control" aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Senha</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['senha'] ?>" name="senha" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Desconto Geral</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['codigo'] ?>" name="codigo" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Google Analitycs</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['senha'] ?>" name="senha" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="label"><strong><span>Facebook</span></strong></div>
                                        <div class="form-group">
                                            <input type="text" value="<?php echo $field['codigo'] ?>" name="codigo" class="form-control"  aria-describedby="text" placeholder="texto">
                                        </div>
                                    </div>
                                </div>
                                <div class="button">
                                    <button type="submit" name="save-info">Salvar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
