<?php 
    if(isset($_GET['page'])){
        require '../../../data/connect_db.php';
    }

    $consulta = $pdo->query("SELECT * FROM ig_produtos ");
?>

<div class="product-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-0">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="ousada_development-status mb-4">
                                <div class="ousada_development-qtd">
                                    <div class="ousada_development-qtd-text">
                                        <span><?php echo $consulta ->rowCount()?> &nbsp; Produtos Totais</span>
                                    </div>
                                </div>
                                <div class="ousada_development-tags">
                                    <div class="ousada_development-tags-item" data-search="todos">
                                        <span>Todos</span>
                                    </div>
                                    <div class="ousada_development-tags-item" data-search="privacidade">
                                        <span>Privacidade</span>
                                    </div>
                                    <div class="ousada_development-tags-item" data-search="disponibilidade">
                                        <span>Disponibilidade</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ousada_development-table">
                                <table id="lista">
                                    <thead>
                                        <tr>
                                            <th>ITENS</th>
                                            <th>PRODUTO</th>
                                            <th>TAMANHOS</th>
                                            <th>CATEGORIAS</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            while ($field = $consulta->fetch(PDO::FETCH_ASSOC)) {?>
                                        <tr>
                                            <td><input type="radio" name="id" value="<?php echo $field['id_produto']?>" ></td>
                                            <td>
                                                <div class="ousada_table-box">
                                                    <div class="ousada_table-box-image">
                                                        <div class="swiper-container">
                                                            <!-- Wrapper adicional necessário -->
                                                            <div class="swiper-wrapper">
                                                                <!-- Slides -->
                                                                <?php
                                                                $srcs = explode(";", $field['img']);
                                                                if(isset($srcs)){
                                                                    foreach($srcs as $src){
                                                                        if(!empty($src)){ 
                                                                ?>
                                                                <div class="swiper-slide">
                                                                    <img src="<?php echo $src ?>" alt="Produto da Moda Ousada">
                                                                </div>
                                                                <?php
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ousada_table-box-info">
                                                        <div class="ousada_table-box-info-name">
                                                            <h4><strong ><span data-name><?php echo $field['nome']?></span></strong></h4>
                                                        </div>
                                                        <div class="ousada_table-box-info-preco">
                                                            <p>R$: <span><?php echo $field['preco']?></span></p>
                                                        </div>
                                                        <div class="ousada_table-box-info-privacidade">
                                                            <p><span data-privacidade><?php if($field['privacidade'] == 1){ echo "privado";}else{echo "Não privado";} ?></span> //  <span data-disponibilidade><?php if($field['disponibilidade'] == 1){ echo "Disponível";}else{echo "Indisponível";} ?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <span><?php echo $field['tamanho']?></span>
                                            </td>
                                            <td>
                                                <span><?php echo $field['categoria']?></span>
                                            </td>
                                            <td>
                                                <a href="./data/system.php?delete&id=<?php echo $field['id_produto'] ?>">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt 
                                                    fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 
                                                    48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 
                                                    16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 
                                                    48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path></svg>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="ousada_development-mob">
                                <div class="ousada_development-mob-list">
                                    <ul>
                                        <?php 
                                        $consulta = $pdo->query("SELECT * FROM ig_produtos ");
                                            while ($field = $consulta->fetch(PDO::FETCH_ASSOC)) {?>
                                        <li>
                                            <div class="list--mob-select">
                                                <div class="list--mob-select-input">
                                                    <input type="radio" name="id" onclick="addLink(this, <?php echo $dados['id_produtos'] ?>);" >
                                                </div>
                                            </div>
                                            <div class="list--mob-box">
                                                <div class="list--mob-box-img">
                                                    <div class="swiper-container">
                                                        <!-- Wrapper adicional necessário -->
                                                        <div class="swiper-wrapper">
                                                            <!-- Slides -->
                                                            <?php
                                                            $srcs = explode(";", $field['img']);
                                                            if(isset($srcs)){
                                                                foreach($srcs as $src){
                                                                    if(!empty($src)){  
                                                            ?>
                                                            <div class="swiper-slide">
                                                                <img src="<?php echo "../../".$src ?>" alt="Produto da Moda Ousada">
                                                            </div>
                                                            <?php
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="list--mob-box-infos">
                                                    <div class="list--mob-box-infos-name">
                                                        <h4><?php echo $field['nome']?></h4>
                                                    </div>
                                                    <div class="list--mob-box-infos-describe">
                                                        <p><Strong>Categoria:</Strong> <?php echo $field['categoria']?> <br> <strong>R$:</strong> <?php echo $field['preco']?> <br> <strong>Disponibilidade/Privacidade:</strong> <span data-disponibilidade><?php if($field['disponibilidade'] == 1){ echo "Disponível";}else{echo "Indisponível";} ?></span>/ <span data-privacidade><?php if($field['privacidade'] == 1){ echo "privado";}else{echo "Não privado";} ?></span> <br><br> <strong>Tamanhos:</strong> <?php echo $field['tamanho']?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<actions>
    <div class="ousada">
        <div class="ousada_actions">
            <div class="ousada_actions-btn">
                <div class="ousada_actions-btn-icon">
                    <a  class="ousada_actions-btn-icon-edit">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tools" class="svg-inline--fa fa-tools fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" 
                        viewBox="0 0 512 512"><path fill="currentColor" d="M501.1 395.7L384 278.6c-23.1-23.1-57.6-27.6-85.4-13.9L192 158.1V96L64 0 0 64l96 128h62.1l106.6 106.6c-13.6 27.8-9.2 
                        62.3 13.9 85.4l117.1 117.1c14.6 14.6 38.2 14.6 52.7 0l52.7-52.7c14.5-14.6 14.5-38.2 0-52.7zM331.7 225c28.3 0 54.9 11 74.9 31l19.4 19.4c15.8-6.9 30.8-16.5 43.8-29.5 
                        37.1-37.1 49.7-89.3 37.9-136.7-2.2-9-13.5-12.1-20.1-5.5l-74.4 74.4-67.9-11.3L334 98.9l74.4-74.4c6.6-6.6 3.4-17.9-5.7-20.2-47.4-11.7-99.6.9-136.6 37.9-28.5 28.5-41.9 
                        66.1-41.2 103.6l82.1 82.1c8.1-1.9 16.5-2.9 24.7-2.9zm-103.9 82l-56.7-56.7L18.7 402.8c-25 25-25 65.5 0 90.5s65.5 25 90.5 0l123.6-123.6c-7.6-19.9-9.9-41.6-5-62.7zM64 
                        472c-13.2 0-24-10.8-24-24 0-13.3 10.7-24 24-24s24 10.7 24 24c0 13.2-10.7 24-24 24z"></path></svg>
                    </a>
                </div> 
                <div class="ousada_actions-btn-icon">
                    <a class="ousada_actions-btn-icon-create" data-toggle="modal" data-target="#CreateProduct">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tags" class="svg-inline--fa fa-tags fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M497.941 225.941L286.059 14.059A48 48 0 0 0 252.118 0H48C21.49 0 0 21.49 0 48v204.118a48 48 0 0 0 14.059 33.941l211.882 211.882c18.744 18.745 49.136 18.746 67.882 0l204.118-204.118c18.745-18.745 18.745-49.137 0-67.882zM112 160c-26.51 0-48-21.49-48-48s21.49-48 48-48 48 21.49 48 48-21.49 48-48 48zm513.941 133.823L421.823 497.941c-18.745 18.745-49.137 18.745-67.882 0l-.36-.36L527.64 323.522c16.999-16.999 26.36-39.6 26.36-63.64s-9.362-46.641-26.36-63.64L331.397 0h48.721a48 48 0 0 1 33.941 14.059l211.882 211.882c18.745 18.745 18.745 49.137 0 67.882z"></path></svg>
                    </a>      
                </div>
            </div>
        </div>
    </div>
</actions>    
   