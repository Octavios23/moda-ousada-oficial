<?php 

if(isset($_GET['save-info'])){

    $nome = htmlspecialchars($_GET['nome']);
    $user = htmlspecialchars($_GET['user']);
    $telefone = htmlspecialchars($_GET['telefone']);
    $email = htmlspecialchars($_GET['email']);
    $senha = htmlspecialchars($_GET['senha']);
    $codigo = htmlspecialchars($_GET['codigo']);
    

    try{

        $stmt = $pdo->prepare('INSERT INTO ig_adminstrador (`nome`, `user`, `telefone`, `email`, `senha`, `codigo`) VALUES (:nome, :user, :telefone, :email, :senha, :codigo)');
        $stmt->execute(array(
            ':nome' => $nome,
            ':user' => $user,
            ':telefone' => $telefone,
            ':email' => $email,
            ':senha' => $senha,
            ':codigo' => $codigo
        ));
      
        if($stmt->rowCount() > 0){
            echo "Salvo com Sucesso! :)";
        }else{
            echo "Ocorreu algum problema! =/";
        }
    }catch(PDOException $e){
        echo 'Error: ' . $e->getMessage();
    }
}

?>