<?php 

if(isset($_POST['edit'])){
 
    $nome = ucfirst(mysqli_escape_string($connect, $_POST['nome']));

    $imgM = '';$imgOld = '';
    

    $preco = mysqli_escape_string($connect, $_POST['preco']);

    $id = mysqli_escape_string($connect, $_POST['id']);

    
    $sql = "SELECT img FROM ig_produtos WHERE id_produtos = '$id'";

    $result = mysqli_query($connect, $sql); //EXCLUIR TODAS AS IMAGENS ANTIGAS
    $imgOld = mysqli_fetch_row($result);
    if(isset($_POST['delIMG'])):
        $imgOld = explode(';', $imgOld[0]);
        for($x = 0; $x < count($imgOld); $x++):
            $val = $imgOld[$x];
            unlink($val);
        endfor;

        $formatosPermitidos = array("png", "jpeg", "jpg", "gif" ); //ADICIONAR NOVAS IMAGENS
        $quantidadeArquivos = count($_FILES['arquivo']['name']); //A função count irá contar. 
        //A função "$_FILES" irá pegar o elemento para ser contado pelo seu valor(arquivo) e o nome do atributo(name)
        $contador = 0;
    
        $_SESSION['mensagem'] = $_FILES['arquivo']['error'];;
        while($contador < $quantidadeArquivos):
            $_FILES['arquivo']['error'];
            $extensao = pathinfo($_FILES['arquivo']['name'][$contador], PATHINFO_EXTENSION); // O pathinfo irá dar informações sobre um determinado elemento. 
            //Na função $_FILES foi necessário colocar a variável 'contador' para modificar os indices do array.

            if(in_array($extensao, $formatosPermitidos)):
                $pasta = "../img/upload/" ;//Aqui voce coloca o nome da pasta.
                $temporarios = $_FILES['arquivo']['tmp_name'][$contador]; //Aqui foi selecionado o arquivo que irá ser salvo.
                $novoNome =  uniqid().".$extensao"; //Aqui foi criado o nome, deixando claro que a função 'uniqid()' serve para especificar que será apenas esse elemento com esse id, sem repetições.  
                move_uploaded_file($temporarios, $pasta.$novoNome);
                /*
                if()://Vai confirmar se o arquivo foi enviado ou não para dentro da pasta especificada. 
                    // O primeiro parâmetro é o arquivo, em seguida é o nome da pasta.novonomedoarquivo
                    $_SESSION['mensagem'] = "Upload feito com sucesso $pasta.$novoNome <br>";
                    
                else:
                    $_SESSION['mensagem'] = "Erro ao enviar o arquivo $temporarios";
                endif;*/

                $imgM = $pasta.$novoNome.';'.$imgM;
            
            else: 
                $_SESSION['mensagem'] = "A imagem contém um Formato inválido";
            endif;

            $contador++;
        endwhile;

        $imgOld = $imgM;
    else: 

        $formatosPermitidos = array("png", "jpeg", "jpg", "gif" ); //ADICIONAR NOVAS IMAGENS
        $quantidadeArquivos = count($_FILES['arquivo']['name']); //A função count irá contar. 
        //A função "$_FILES" irá pegar o elemento para ser contado pelo seu valor(arquivo) e o nome do atributo(name)
        $contador = 0;
    
        $_SESSION['mensagem'] = $_FILES['arquivo']['error'];;
        while($contador < $quantidadeArquivos):
            $_FILES['arquivo']['error'];
            $extensao = pathinfo($_FILES['arquivo']['name'][$contador], PATHINFO_EXTENSION); // O pathinfo irá dar informações sobre um determinado elemento. 
            //Na função $_FILES foi necessário colocar a variável 'contador' para modificar os indices do array.

            if(in_array($extensao, $formatosPermitidos)):
                $pasta = "../img/upload/" ;//Aqui voce coloca o nome da pasta.
                $temporarios = $_FILES['arquivo']['tmp_name'][$contador]; //Aqui foi selecionado o arquivo que irá ser salvo.
                $novoNome =  uniqid().".$extensao"; //Aqui foi criado o nome, deixando claro que a função 'uniqid()' serve para especificar que será apenas esse elemento com esse id, sem repetições.  
                move_uploaded_file($temporarios, $pasta.$novoNome);
                /*
                if()://Vai confirmar se o arquivo foi enviado ou não para dentro da pasta especificada. 
                    // O primeiro parâmetro é o arquivo, em seguida é o nome da pasta.novonomedoarquivo
                    $_SESSION['mensagem'] = "Upload feito com sucesso $pasta.$novoNome <br>";
                    
                else:
                    $_SESSION['mensagem'] = "Erro ao enviar o arquivo $temporarios";
                endif;*/

                $imgM = $pasta.$novoNome.';'.$imgM;
            
            else: 
                $_SESSION['mensagem'] = "A imagem contém um Formato inválido";
            endif;

            $contador++;
        endwhile;


        $imgOld = $imgM.";".$imgOld[0];
        
    endif;


    $sizeTot = '';$z = 1;

    $size = $_POST['tamanho'];
    //Para cada checkbox selecionado
    foreach($size as $sizeV){
        if($z == 1){
            $sizeTot .= mysqli_escape_string($connect,$sizeV);
        }else{
            $sizeTot .= ','.mysqli_escape_string($connect,$sizeV);
        }
        $z++;
    }


    $categTot = '';$z = 1;

    $categoria =  $_POST['categoria'];
    //Para cada checkbox selecionado
    foreach($categoria as $categ){
        if($z == 1){
            $categTot .= mysqli_escape_string($connect,$categ);
        }else{
            $categTot .= ','.mysqli_escape_string($connect,$categ);
        }
        $z++;
    }


    try{

        $stmt = $pdo->prepare('UPDATE ig_produtos SET  nome = nome, tamanho = :tamanho, preco = :preco, img = :img, categoria = :categoria, disponibilidade = :disponibilidade, privacidade = :privacidade WHERE id_produtos = :id');
        $stmt->execute(array(
            ':id' => $id,
            ':nome' => $nome,
            ':tamanho' => $sizeTot,
            ':preco' => $preco,
            ':img' => $imgOld,
            ':categoria' => $categTot,
            ':disponibilidade' => $disponibilidade,
            ':privacidade' => $privacidade
        ));
      
        if($stmt->rowCount() > 0){
            $_SESSION['mensagem'] = "Atualizado com sucesso!";
            header('Location: ../php_action/update.php?sucesso');
        }else{
            $_SESSION['mensagem'] = "Erro ao Atualizar!";
            header('Location: ../php_action/update.php?fracasso');
        }
    }catch(PDOException $e){
        echo 'Error: ' . $e->getMessage();
    }
}
    
?>