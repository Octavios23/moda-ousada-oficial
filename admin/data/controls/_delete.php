<?php 

if(isset($_GET['delete'])){
    $id = $_GET['id'];

 
    try{
        $stmt = $pdo->prepare('DELETE FROM ig_produtos WHERE id_produto = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        
        echo $stmt->rowCount();

        header('Location: ../admin-dashboard.php?sucesso'); // Irá direcionar e mostrar na URL uma mensagem de sucesso.
        
    }catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
        $_SESSION['mensagem'] = "Erro ao deletar!";
        header("Location: ../admin-dashboard.php?erro$id"); // Irá direcionar e mostrar na URL uma mensagem de erro.
    }

}


?>