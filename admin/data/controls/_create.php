<?php 

if(isset($_POST['create'])){
    $nome = ucfirst(htmlspecialchars(ucwords($_POST['name'])));
    $preco = htmlspecialchars(ucwords($_POST['price']));
    $disponibilidade = htmlspecialchars(ucwords($_POST['disponibilidade']));
    $privacidade = htmlspecialchars(ucwords($_POST['privacidade']));

    $sizeTot = '';$z = 1;

    $size = $_POST['tamanho'];
    //Para cada checkbox selecionado
    foreach($size as $sizeV){
        if($z == 1){
            $sizeTot .= $sizeV;
        }else{
            $sizeTot .= ','.$sizeV;
        }
        $z++;
    }


    $categTot = '';$z = 1;

    $categoria =  $_POST['categoria'];
    //Para cada checkbox selecionado
    foreach($categoria as $categ){
        if($z == 1){
            $categTot .= $categ;
        }else{
            $categTot .= ','.$categ;
        }
        $z++;
    }


    $formatosPermitidos = array("png", "jpeg", "jpg", "gif" );
    $quantidadeArquivos = count($_FILES['arquivo']['name']); //A função count irá contar. 
    //A função "$_FILES" irá pegar o elemento para ser contado pelo seu valor(arquivo) e o nome do atributo(name)
    $contador = 0;
  
    $_SESSION['mensagem'] = $_FILES['arquivo']['error'];;
    while($contador < $quantidadeArquivos):
        $_FILES['arquivo']['error'];
        $extensao = pathinfo($_FILES['arquivo']['name'][$contador], PATHINFO_EXTENSION); // O pathinfo irá dar informações sobre um determinado elemento. 
        //Na função $_FILES foi necessário colocar a variável 'contador' para modificar os indices do array.

        if(in_array($extensao, $formatosPermitidos)):
            $pasta = "../assets/upload/" ;//Aqui voce coloca o nome da pasta.
            $temporarios = $_FILES['arquivo']['tmp_name'][$contador]; //Aqui foi selecionado o arquivo que irá ser salvo.
            $novoNome =  uniqid().".$extensao"; //Aqui foi criado o nome, deixando claro que a função 'uniqid()' serve para especificar que será apenas esse elemento com esse id, sem repetições.  
            move_uploaded_file($temporarios, $pasta.$novoNome);
            /*
            if()://Vai confirmar se o arquivo foi enviado ou não para dentro da pasta especificada. 
                // O primeiro parâmetro é o arquivo, em seguida é o nome da pasta.novonomedoarquivo
                $_SESSION['mensagem'] = "Upload feito com sucesso $pasta.$novoNome <br>";
                
            else:
                $_SESSION['mensagem'] = "Erro ao enviar o arquivo $temporarios";
            endif;*/

            $imgM = $pasta.$novoNome.';'.$imgM;
        
        else: 
            $_SESSION['mensagem'] = "A imagem contém um Formato inválido";
        endif;

        $contador++;
    endwhile;


    try{

        $stmt = $pdo->prepare('INSERT INTO ig_produtos (`nome`, `tamanho`, `preco`, `img`, `categoria`, `disponibilidade`, `privacidade`) VALUES (:nome, :tamanho, :preco, :img, :categoria, :disponibilidade, :privacidade)');
        $stmt->execute(array(
            ':nome' => $nome,
            ':tamanho' => $sizeTot,
            ':preco' => $preco,
            ':img' => $imgM,
            ':categoria' => $categTot,
            ':disponibilidade' => $disponibilidade,
            ':privacidade' => $privacidade
        ));
      
        if($stmt->rowCount() > 0){
            header("Location: ../admin-dashboard.php?sucesso"); 
        }else{
            header("Location: ../admin-dashboard.php?erro");
        }
    }catch(PDOException $e){
        echo 'Error: ' . $e->getMessage();
    }
}
    
?>