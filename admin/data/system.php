<?php 
// Start Session
session_start();

//CALL BD
require_once './../../data/connect_db.php';


//SAVE INFO
require './info/info.php';


// SIGN
require './sign/sign-in.php';
require './sign/sign-out.php';


// SYSTEM PRODUCTS
require './controls/_delete.php'; 
require './controls/_create.php'; 
require './controls/_edit.php'; 


?>