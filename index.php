<?php 

//INCLUDES 
include_once 'header.php';

?>
<div class="content">

    <!-- HEADER -->
    <?php require_once './parts/part-header.php' ?>

    <!-- MAIN -->
    <?php require_once './parts/part-main.php' ?>

    <!-- FIRST SECTION -->
    <?php require_once './parts/part-banners.php' ?>

    <!-- PRODUCT SECTION -->
    <?php require_once './parts/part-product.php' ?>

    <!-- SECOND SECTION -->
    <?php require_once './parts/part-promotions.php' ?>

    <!-- ASIDE -->
    <?php require_once './parts/part-footer.php' ?>

    <!-- CART -->
    <?php require_once './parts/part-cart.php'?>

</div>




    <!-- FOOTER -->

<?php 

    include_once 'footer.php';

?>