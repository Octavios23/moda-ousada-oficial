<?php 

require '../../data/connect_db.php';
require '../mail/mail.php';
require './html_mail/html.php';

if(isset($_POST['inscribe'])){

    $email = ucfirst(htmlspecialchars(ucwords($_POST['email'])));
    $telefone = ucfirst(htmlspecialchars(ucwords($_POST['telefone'])));
    $localizacao = ucfirst(htmlspecialchars(ucwords($_POST['coordenadas'])));

    try{

        $stmt = $pdo->prepare('INSERT INTO ig_clientes (`email`, `celular`, `terms`, `localizacao`) VALUES (:email, :celular, :terms, :localizacao)');
        $stmt->execute(array(
            ':email' => $email,
            ':celular' => $telefone,
            ':terms' => "Aceito",
            ':localizacao' => $localizacao
        ));
      
        if($stmt->rowCount() > 0){
            echo "sucess";

            $infos = (object)[
                'title' => 'Boas Vindas - Agradecemos a sua inscrição',
                'destiny_mail' => $email,
                'destiny_name' => 'New Cliente',
                'html' => $html,
                'html_no' => $no_html,
              ];

            sendEmail($infos);
        }else{
            echo "failed";
        }
    }catch(PDOException $e){
        echo 'Error: ' . $e->getMessage();
    }
}
?>