<?php 

include_once '../../data/connect_db.php';


if(isset($_REQUEST['categ'])){

    if($_REQUEST['categ'] != NULL){
        $categ = htmlspecialchars($_REQUEST['categ']);
    }

    $cmd = $pdo->query("SELECT * FROM ig_produtos WHERE categoria LIKE '%$categ%' ");

    if($cmd->rowCount() > 0):
        while($dados = $cmd->fetch(PDO::FETCH_ASSOC)):
    ?>
        <div class="info-box product mx-auto mt-5 fade-in" style="display: inline;" data-serie="<?php echo $dados['id_produtos']?>" data-wish="no">
            <div class="box-img" >
                <?php
                    
                    $url = explode(';', $dados['img']);
                    if(count($url) > 0): #Se a estrutura '$result' possuir linhas de parâmtros maiores que 0, vai ser feito:
                        for($x = 0 ;$x < count($url); $x++): #Enquanto ainda houver valores no array(linhas) na variável 'result' ele vai produzir a estrutura abaixo;
                            $url[$x] = substr($url[$x], 3);
                            if($url[$x] != ''){
                                echo '<img src="'.$url[$x].'" alt="img'.$x.'" data-settings>';
                                if($x == 0){
                                    $_SESSION["loc"] = 2;
                                }
                                else{
                                    $_SESSION["loc"] = $x + 3;
                                }
                            }
                        endfor;
                    endif;
                ?> 
            </div>
            <div class="box-title">
                <span data-product="name"><?php echo $dados['nome']?></span><br>
                <s class="text-danger" >R$:<span data-product="price"><?php 
                    $valSub = $dados['preco']+(($dados['preco'] /100)*15);
                    echo number_format($valSub, 2, ',', '.');
                
                ?></span></s>
                <span class="separador"> - </span>
                R$:<span data-product="price"><?php echo number_format($dados['preco'], 2, ',', '.');?></span>
            </div>


            <?php 
                $P = '';$M = ''; $G = '';$GG = ''; $XG = '';
                $archive = explode(",", $dados['tamanho']);
                for($x = 0; $x < count($archive); $x++){
                    if(stristr($archive[$x],'Pequeno')){
                        $P = 'exists';
                    }
                    if(stristr($archive[$x],'Medio')){
                        $M = 'exists';
                    }
                    if(stristr($archive[$x],'Extra Grande')){
                        $GG = 'exists';
                    }else if(stristr($archive[$x],'Extra x Grande')){
                        $XG = 'exists';
                    }else if(stristr($archive[$x],'Grande')){
                        $G = 'exists';
                    }
                }
            ?>


            <div class="box-sizes my-2" data-sizes="<?php echo $dados['tamanho']?>">

                <div class="size <?php echo $P ?>">
                    <span>P</span>
                    <span class="boll">Especifique os tamanhos que deseja no ato da compra</span>
                </div>
                <div class="size <?php echo $M ?>">
                    <span>M</span>
                </div>
                <div class="size <?php echo $G ?>">
                    <span>G</span>
                </div>
                <div class="size <?php echo $GG ?>">
                    <span>GG</span>
                </div>
                <div class="size <?php echo $XG ?>">
                    <span>XG</span>
                </div>
            </div>
            <div class="box-end" >
                <div class="value">
                    <input min="1" type="number" name="qtd" data-product="values">
                </div>
                <div class="btn-add" onclick="cartAction(this)" data-iten="<?php echo $dados['id_produtos']?>">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cart-plus" class="svg-inline--fa fa-cart-plus fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M504.717 320H211.572l6.545 32h268.418c15.401 0 26.816 14.301 23.403 29.319l-5.517 24.276C523.112 414.668 536 433.828 536 456c0 31.202-25.519 56.444-56.824 55.994-29.823-.429-54.35-24.631-55.155-54.447-.44-16.287 6.085-31.049 16.803-41.548H231.176C241.553 426.165 248 440.326 248 456c0 31.813-26.528 57.431-58.67 55.938-28.54-1.325-51.751-24.385-53.251-52.917-1.158-22.034 10.436-41.455 28.051-51.586L93.883 64H24C10.745 64 0 53.255 0 40V24C0 10.745 10.745 0 24 0h102.529c11.401 0 21.228 8.021 23.513 19.19L159.208 64H551.99c15.401 0 26.816 14.301 23.403 29.319l-47.273 208C525.637 312.246 515.923 320 504.717 320zM408 168h-48v-40c0-8.837-7.163-16-16-16h-16c-8.837 0-16 7.163-16 16v40h-48c-8.837 0-16 7.163-16 16v16c0 8.837 7.163 16 16 16h48v40c0 8.837 7.163 16 16 16h16c8.837 0 16-7.163 16-16v-40h48c8.837 0 16-7.163 16-16v-16c0-8.837-7.163-16-16-16z"></path></svg>
                    <button>Adicionar ao carrinho</button>
                </div>
            </div>
        </div>

    <?php 

    endwhile;
    else:
    ?>
        <div class="box-alert mx-auto">
            <div class="box-text">
                <h1>Desculpe, mas o nosso estoque acabou!</h1>
            </div>
        </div>
    
        <?php 
    endif;
}else{

    $cmd = $pdo->query("SELECT * FROM ig_produtos WHERE categoria LIKE '%Os Mais Vendidos%' ");

    if($cmd->rowCount() > 0):
        while($dados = $cmd->fetch(PDO::FETCH_ASSOC)):

    ?>
        <div class="info-box mx-auto mt-5" style="display: inline;" data-serie="<?php echo $dados['id_produtos']?>">
            <div class="box-img">
                <?php
                    
                    $url = explode(';', $dados['img']);
                    if(count($url) > 0): #Se a estrutura '$result' possuir linhas de parâmtros maiores que 0, vai ser feito:
                        for($x = 0 ;$x < count($url); $x++): #Enquanto ainda houver valores no array(linhas) na variável 'result' ele vai produzir a estrutura abaixo;
                            $url[$x] = substr($url[$x], 3);
                            if($url[$x] != ''){
                                echo '<img src="'.$url[$x].'" alt="img'.$x.'" data-settings>';
                                if($x == 0){
                                    $_SESSION["loc"] = 2;
                                }
                                else{
                                    $_SESSION["loc"] = $x + 3;
                                }
                            }
                        endfor;
                    endif;
                ?> 
            </div>
            <div class="box-title">
                <span><?php echo $dados['nome']?></span>
            </div>
            <div class="box-sizes my-2" data-sizes="<?php echo $dados['tamanho']?>">

                <?php 
                    $P = '';$M = ''; $G = '';$GG = ''; $XG = '';
                    //$archive = explode(",", $dados['categoria']);

                    if(stristr($dados['categoria'],'P')){
                        $P = 'exists';
                    }
                    if(stristr($dados['categoria'],'M')){
                        $M = 'exists';
                    }
                    if(stristr($dados['categoria'],'G')){
                        $G = 'exists';
                    }
                    if(stristr($dados['categoria'],'GG')){
                        $GG = 'exists';
                    }
                    if(stristr($dados['categoria'],'XG')){
                        $XG = 'exists';
                    }
                ?>

                <div class="size <?php echo $P ?>">
                    <span>P</span>
                    <span class="boll">Especifique os tamanhos que deseja no ato da compra</span>
                </div>
                <div class="size <?php echo $M ?>">
                    <span>M</span>
                </div>
                <div class="size <?php echo $G ?>">
                    <span>G</span>
                </div>
                <div class="size <?php echo $GG ?>">
                    <span>GG</span>
                </div>
                <div class="size <?php echo $XG ?>">
                    <span>XG</span>
                </div>
            </div>
            <div class="box-end">
                <div class="value">
                    <input min="1" type="number" name="" id="">
                </div>
                <div class="btn-add" onclick="cartAction(this)" data-iten="<?php echo $dados['id_produtos']?>">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cart-plus" class="svg-inline--fa fa-cart-plus fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M504.717 320H211.572l6.545 32h268.418c15.401 0 26.816 14.301 23.403 29.319l-5.517 24.276C523.112 414.668 536 433.828 536 456c0 31.202-25.519 56.444-56.824 55.994-29.823-.429-54.35-24.631-55.155-54.447-.44-16.287 6.085-31.049 16.803-41.548H231.176C241.553 426.165 248 440.326 248 456c0 31.813-26.528 57.431-58.67 55.938-28.54-1.325-51.751-24.385-53.251-52.917-1.158-22.034 10.436-41.455 28.051-51.586L93.883 64H24C10.745 64 0 53.255 0 40V24C0 10.745 10.745 0 24 0h102.529c11.401 0 21.228 8.021 23.513 19.19L159.208 64H551.99c15.401 0 26.816 14.301 23.403 29.319l-47.273 208C525.637 312.246 515.923 320 504.717 320zM408 168h-48v-40c0-8.837-7.163-16-16-16h-16c-8.837 0-16 7.163-16 16v40h-48c-8.837 0-16 7.163-16 16v16c0 8.837 7.163 16 16 16h48v40c0 8.837 7.163 16 16 16h16c8.837 0 16-7.163 16-16v-40h48c8.837 0 16-7.163 16-16v-16c0-8.837-7.163-16-16-16z"></path></svg>
                    <button >Adicionar ao carrinho</button>
                </div>
            </div>
        </div>

    <?php 

    endwhile;
    endif;
}
?>
