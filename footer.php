
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 pt-4 pb-2" style="background-color: #840731;">
                <div class="container">
                    <div class="row justify-content-center justify-content-md-start">
                        <div class="col-12 col-md-5 text-white">
                            <div class="box-text text-center animate-13" data-action>
                                <p>&copy;Copyright - All right's reserve ModaOusada - Develop <span>iGear</span></p>
                            </div>
                        </div>
                        <div class="col-6 col-md-2 ms-md-auto p-0">
                            <div class="box-img p-0 m-0 animate-14" data-action>
                                <img class="w-100  bg-white px-2" src="./assets/img/cartoes.png" alt="Faixa de catões aceitos" style="border-radius: 4px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>    


    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            document.querySelector('#categ-on').click()
        });
    </script>

    <!-- Libarys -->
    <script src="./assets/js/libarys/jQuery/jquery-3.6.0.min.js"></script>
    <script src="./assets/js/libarys/bootStrap/bootstrap.min.js"></script>
    <script src="./assets/js/libarys/owlCarousel/owl.carousel.min.js"></script>
    <script src="./assets/js/libarys/owlCarousel/OwlCarousel.action.js"></script>
    <script src="./assets/js/libarys/mask/jquery.mask.min.js"></script>
    <script src="./assets/js/libarys/tellinput/intlTelInput.min.js"></script>

    <!-- Actions -->
    <script type="module" src="./assets/js/template-ousada.js"></script>





</body>
</html>